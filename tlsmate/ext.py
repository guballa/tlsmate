# -*- coding: utf-8 -*-
"""Module containing the TLS Extension classes
"""

# import basic stuff
import abc
import logging
from dataclasses import dataclass, field, InitVar
from typing import Any, Optional, List, Union, ClassVar, Mapping, Type

# import own stuff
import tlsmate.client_state as client_state
import tlsmate.tls as tls
import tlsmate.pdu as pdu

# import other stuff


class Extension(metaclass=abc.ABCMeta):
    """Abstract class for all extensions."""

    extension_id: ClassVar[tls.Extension]
    """The extension id
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "Extension":
        return cls()

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        pass

    def _encode_extension(
        self, encoder: pdu.Encoder, session: client_state.SessionState
    ) -> None:
        ext_encoder = pdu.Encoder()
        self._encode(ext_encoder, session)
        ext_encoder.add_own_length(width=2)
        ext_encoder.enum(self.extension_id, width=2)
        ext_encoder.prepend_last_item()
        encoder.append_encoder(ext_encoder)


def encode_extensions(
    extensions: List[Extension],
    encoder: pdu.Encoder,
    session: client_state.SessionState,
) -> None:
    ext_encoder = pdu.Encoder()
    for extension in extensions:
        extension._encode_extension(ext_encoder, session)
        if extension.extension_id is tls.Extension.PRE_SHARED_KEY:
            ext_encoder.set_marker()

    session.bytes_after_psk = ext_encoder.distance_to_marker()
    ext_encoder.add_own_length(width=2)
    encoder.append_encoder(ext_encoder)


def decode_extension(
    decoder: pdu.Decoder, session: client_state.SessionState
) -> Extension:
    ext_id = decoder.enum(width=2, cls=tls.Extension)
    ext_decoder = pdu.Decoder(decoder.octets(width=2))
    cls = deserialization_map.get(ext_id)
    if not cls:
        return UnknownExtension(id=ext_id, bytes=ext_decoder.octets())

    try:
        extension = cls._decode(ext_decoder, session)

    except tls.ServerMalfunction as exc:
        # enrich the exception with the message id and extension id
        exc.malfunction.message = session.xcoding_msg
        exc.malfunction.extension = ext_id
        raise exc

    if ext_decoder.get_length():
        raise tls.ServerMalfunction(
            tls.ServerIssue.EXTENSION_LENGTH_TOO_LONG,
            message=session.xcoding_msg,
            extension=ext_id,
        )

    return extension


def decode_extensions(
    decoder: pdu.Decoder, session: client_state.SessionState
) -> List[Extension]:
    if decoder.get_length():
        return [
            decode_extension(decoder, session)
            for _ in decoder.loop_length_field(width=2)
        ]

    return []


@dataclass
class UnknownExtension(Extension):
    """Any extension which is not known by tlsmate (yet).

    E.g.,

    >>> Heartbeat(mode=tls.HeartbeatMode.PEER_ALLOWED_TO_SEND)

    encodes to "00 0f 00 01 01", where "00 0f" represents the extension id, and the
    following two bytes "00 01" repesent the length of the extension.

    This is equivalent to:

    >>> UnknownExtension(id=15, bytes=b"\\x01")
    """

    extension_id: ClassVar = tls.Extension.UNKNOW_EXTENSION
    """:obj:`tlsmate.tls.Extension.UNKNOW_EXTENSION`
    """

    id: int
    """The id of the extension
    """

    bytes: bytes
    """The content of the extension
    """

    def _encode_extension(
        self, encoder: pdu.Encoder, session: client_state.SessionState
    ) -> None:
        encoder.uint16(self.id)
        encoder.octets(self.bytes, width=2)


@dataclass
class ServerName(object):
    """Structure part of the ServerNameIndication

    Arbitrary host types can be given by using an integer value:

    >>> ServerName(name_type=3, name="localhost")
    ServerName(name_type=3, name='localhost')
    """

    name_type: Union[tls.SniHostType, int] = tls.SniHostType.HOST_NAME
    """The type of the entry, currently, the standard only defines the type "host_name".
    """

    name: str = ""
    """The name of this entry.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ServerName":
        return cls(
            name_type=decoder.enum(width=1, cls=tls.SniHostType),
            name=decoder.string(width=2),
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(self.name_type, width=1)
        encoder.string(self.name, width=2)


@dataclass
class ServerNameIndication(Extension):
    """Represents the ServerNameIndication extension."""

    extension_id: ClassVar = tls.Extension.SERVER_NAME
    """:obj:`tlsmate.tls.Extension.SERVER_NAME`
    """

    host_name: InitVar[str] = None
    """This field is only applicable when instantiating the class.

    It allows to setup an entry in the server_name_list of the type
    :obj:`tlsmate.tls.SniHostType.HOST_NAME` with the given host name.

    >>> ServerNameIndication(host_name="localhost")
    ServerNameIndication(server_name_list=[ServerName(name_type=<SniHostType.HOST_NAME: 0>, name='localhost')])
    """  # noqa

    server_name_list: List[ServerName] = field(default_factory=list)
    """The list of :obj:`ServerName` objects. Typically, this list contains
    just one entry with the type :obj:`tlsmate.tls.SniHostType.HOST_NAME`.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ServerNameIndication":
        if decoder.get_length():
            return cls(
                server_name_list=[
                    ServerName._decode(decoder, session)
                    for _ in decoder.loop_length_field(width=2)
                ]
            )

        # Server may respond with an empty extension
        return cls()

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        for server_name in self.server_name_list:
            server_name._encode(encoder, session)

        encoder.add_own_length(width=2)

    def __post_init__(self, hostname):
        if hostname:
            self.server_name_list.append(ServerName(name=hostname))

    def get_host_name(self) -> Optional[str]:
        """Comfortable way to savely extract the host name

        >>> sni = ServerNameIndication(host_name="localhost")
        >>> sni.get_host_name()
        'localhost'
        """

        for server_name in self.server_name_list:
            if server_name.name_type is tls.SniHostType.HOST_NAME:
                return server_name.name

        return None


@dataclass
class ExtendedMasterSecret(Extension):
    """Represents the ExtendedMasterSecret extension.

    This extension does not have any content.
    """

    extension_id: ClassVar = tls.Extension.EXTENDED_MASTER_SECRET
    """:obj:`tlsmate.tls.Extension.EXTENDED_MASTER_SECRET`
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ExtendedMasterSecret":
        return cls()

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        pass


@dataclass
class EncryptThenMac(Extension):
    """Represents the EncryptThenMac extension.

    This extension does not have any content.
    """

    extension_id: ClassVar = tls.Extension.ENCRYPT_THEN_MAC
    """:obj:`tlsmate.tls.Extension.ENCRYPT_THEN_MAC`
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "EncryptThenMac":
        return cls()

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        pass


@dataclass
class RenegotiationInfo(Extension):
    """Represents the RenegotiationInfo extension.

    E.g.,

    >>> RenegotiationInfo(renegotiated_connection=b"\1")
    RenegotiationInfo(renegotiated_connection=b'\x01')
    """

    extension_id: ClassVar = tls.Extension.RENEGOTIATION_INFO
    """:obj:`tlsmate.tls.Extension.RENEGOTIATION_INFO`
    """

    renegotiated_connection: bytes = b"\0"
    """The opaque bytes.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "RenegotiationInfo":
        return cls(renegotiated_connection=decoder.octets(width=1))

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.octets(self.renegotiated_connection, width=1)


@dataclass
class EcPointFormats(Extension):
    """Represents the EcPointFormat extension.

    E.g.,

    >>> EcPointFormats()
    EcPointFormats(ec_point_format_list=[<EcPointFormat.UNCOMPRESSED: 0>])

    Arbitrary point formats can be specified by their integer value.

    >>> EcPointFormats(ec_point_format_list=[tls.EcPointFormat.UNCOMPRESSED, 5, 17])
    EcPointFormats(ec_point_format_list=[<EcPointFormat.UNCOMPRESSED: 0>, 5, 17])
    """

    extension_id: ClassVar = tls.Extension.EC_POINT_FORMATS
    """:obj:`tlsmate.tls.Extension.EC_POINT_FORMATS`
    """

    ec_point_formats: InitVar[Any] = None
    """This field is only applicable when instantiating the class.

    It is a synonym for ec_point_format_list. It is here for backward compatibility
    only and will be removed in the future.
    """

    ec_point_format_list: List[Union[tls.EcPointFormat, int]] = field(
        default_factory=lambda: [tls.EcPointFormat.UNCOMPRESSED]
    )
    """The list of EC point formats.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "EcPointFormats":
        return cls(
            ec_point_format_list=[
                decoder.enum(width=1, cls=tls.EcPointFormat)
                for _ in decoder.loop_length_field(width=1)
            ]
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.list_length(self.ec_point_format_list, element_width=1, width=1)
        for ec_point_format in self.ec_point_format_list:
            encoder.enum(ec_point_format, width=1)

    def __post_init__(self, ec_point_format):
        if ec_point_format:
            self.ec_point_format_list = ec_point_format


@dataclass
class SupportedGroups(Extension):
    """Represents the SupportedGroup extension.

    E.g.,

    >>> SupportedGroups(named_group_list=[tls.SupportedGroups.SECP256R1, tls.SupportedGroups.X448])
    SupportedGroups(named_group_list=[<SupportedGroups.SECP256R1: 23>, <SupportedGroups.X448: 30>])

    Arbitrary groups can be specified by their integer value.

    >>> SupportedGroups(named_group_list=[128, 130])
    SupportedGroups(named_group_list=[128, 130])
    """  # noqa

    extension_id: ClassVar = tls.Extension.SUPPORTED_GROUPS
    """:obj:`tlsmate.tls.Extension.SUPPORTED_GROUPS`
    """

    supported_groups: InitVar[Any] = None
    """This field is only applicable when instantiating the class.

    It is a synonym for named_group_list. It is here for backward compatibility
    only and will be removed in the future.
    """

    named_group_list: List[Union[tls.SupportedGroups, int]] = field(
        default_factory=list
    )
    """The list of supported named groups.
    """

    # for backward compatibility only.
    @property
    def named_curve_list(self):
        return self.named_group_list

    @named_curve_list.setter
    def named_curve_list(self, value):
        self.named_group_list = value

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "SupportedGroups":
        return cls(
            named_group_list=[
                decoder.enum(width=2, cls=tls.SupportedGroups)
                for _ in decoder.loop_length_field(width=2, min_val=2)
            ]
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.list_length(self.named_group_list, element_width=2, width=2)
        for named_curve in self.named_group_list:
            encoder.enum(named_curve, width=2)

    def __post_init__(self, supported_groups):
        if supported_groups:
            self.named_group_list = supported_groups


@dataclass
class SignatureAlgorithms(Extension):
    """Represents the SignatureAlgorithms extension.

    E.g.,

    >>> SignatureAlgorithms(supported_signature_algorithms=[tls.SignatureScheme.ECDSA_SHA1, tls.SignatureScheme.RSA_PKCS1_SHA1])
    SignatureAlgorithms(supported_signature_algorithms=[<SignatureScheme.ECDSA_SHA1: 515>, <SignatureScheme.RSA_PKCS1_SHA1: 513>])

    Arbitrary groups can be specified by their integer value.

    >>> SignatureAlgorithms(supported_signature_algorithms=[0xdead])
    SignatureAlgorithms(supported_signature_algorithms=[57005])
    """  # noqa

    extension_id: ClassVar = tls.Extension.SIGNATURE_ALGORITHMS
    """:obj:`tlsmate.tls.Extension.SIGNATURE_ALGORITHMS`
    """

    signature_algorithms: InitVar[List[Union[tls.SignatureScheme, int]]] = None
    """This field is only applicable when instantiating the class.

    It is a synonym for supported_signature_algorithms. It is here for
    backward compatibility only and will be removed in the future.
    """

    supported_signature_algorithms: List[Union[tls.SignatureScheme, int]] = field(
        default_factory=list
    )
    """The list of supported signature algorithms.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "SignatureAlgorithms":
        return cls(
            supported_signature_algorithms=[
                decoder.enum(width=2, cls=tls.SignatureScheme)
                for _ in decoder.loop_length_field(width=2, min_val=2)
            ]
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.list_length(
            self.supported_signature_algorithms, element_width=2, width=2
        )
        for algo in self.supported_signature_algorithms:
            encoder.enum(algo, width=2)

    def __post_init__(self, signature_algorithms):
        if signature_algorithms is not None:
            self.supported_signature_algorithms = signature_algorithms


@dataclass
class Heartbeat(Extension):
    """Represents the Heartbeat extension.

    E.g.,

    >>> Heartbeat(mode=tls.HeartbeatMode.PEER_NOT_ALLOWED_TO_SEND)
    Heartbeat(mode=<HeartbeatMode.PEER_NOT_ALLOWED_TO_SEND: 2>)

    Or to setup arbitrary values:

    >>> Heartbeat(mode=42)
    Heartbeat(mode=42)

    Attributes:
        heartbeat_mode: The mode for the heartbeat extension. This is a synonym for
            mode and it will be removed in the future.
    """

    extension_id: ClassVar = tls.Extension.HEARTBEAT
    """:obj:`tlsmate.tls.Extension.HEARTBEAT`
    """

    mode: Union[tls.HeartbeatMode, int] = tls.HeartbeatMode.PEER_ALLOWED_TO_SEND
    """The mode for the heartbeat extension.
    """

    def __init__(self, heartbeat_mode=None, mode=None):
        mode = mode or heartbeat_mode
        self.mode = mode

    # for backward compatibility
    @property
    def heartbeat_mode(self):
        return self.mode

    @heartbeat_mode.setter
    def heartbeat_mode(self, value):
        self.mode = value

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "Heartbeat":
        return cls(mode=decoder.enum(width=1, cls=tls.HeartbeatMode))

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(self.mode, width=1)


@dataclass
class CertificateAuthorities(Extension):
    """Represents the CertificateAuthorities extension."""

    extension_id: ClassVar = tls.Extension.CERTIFICATE_AUTHORITIES
    """:obj:`tlsmate.tls.Extension.CERTIFICATE_AUTHORITIES`
    """

    authorities: List[bytes] = field(default_factory=lambda: [b" "])
    """The list of authorities as bytes in original ASN.1 format.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "CertificateAuthorities":
        return cls(
            authorities=[
                decoder.octets(width=2, min_val=1)
                for _ in decoder.loop_length_field(width=2, min_val=3)
            ]
        )


@dataclass
class SessionTicket(Extension):
    """Represents the SessionTicket extension.

    E.g.,

    >>> SessionTicket(ticket=b"whatever")
    SessionTicket(ticket=b'whatever')
    """

    extension_id: ClassVar = tls.Extension.SESSION_TICKET
    """:obj:`tlsmate.tls.Extension.SESSION_TICKET`
    """

    ticket: bytes = b""
    """The ticket as an octet string.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "SessionTicket":
        return cls(ticket=decoder.octets())

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.octets(self.ticket)


@dataclass
class OCSPStatusRequest(object):
    """Structure part of status_request extension"""

    responder_id_list: List[bytes] = field(default_factory=list)
    """The list of reponder ids as octet strings.
    """

    request_extensions: bytes = b""
    """The extensions as an octet string.

    Note: "Extensions" refers to an ASN1-encoded octet string, not to TLS extensions
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "OCSPStatusRequest":
        return cls(
            responder_id_list=[
                decoder.octets(width=2, min_val=1)
                for _ in decoder.loop_length_field(width=2)
            ],
            request_extensions=decoder.octets(width=2),
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        resp_encoder = pdu.Encoder()
        for responder in self.responder_id_list:
            resp_encoder.octets(responder, width=2)
        resp_encoder.add_own_length(width=2)
        encoder.append_encoder(resp_encoder)
        encoder.octets(self.request_extensions, width=2)


@dataclass
class StatusRequest(Extension):
    """Represents the status_request extension.

    E.g.,

    >>> StatusRequest()
    StatusRequest(status_type=<StatusType.OCSP: 1>, request=OCSPStatusRequest(responder_id_list=[], request_extensions=b''), response=None)
    """  # noqa

    extension_id: ClassVar = tls.Extension.STATUS_REQUEST
    """:obj:`tlsmate.tls.Extension.STATUS_REQUEST`
    """

    responder_ids: InitVar[Any] = None
    """Only used during instantiation to build an OCSPStatusRequest.
    """

    extensions: InitVar[Any] = None
    """Only used during instantiation to build an OCSPStatusRequest.
    """

    status_type: Union[tls.StatusType, int] = tls.StatusType.OCSP
    """The type of the status request. Defaults to :obj:`tlsamte.tls.StatusType.OCSP`
    """

    request: Optional[OCSPStatusRequest] = None
    """Only relevant for requests, holds an :obj:`OCSPStatusRequest` object.
    """

    response: Optional[bytes] = None
    """Only relevant for responses, holds an byte string.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "StatusRequest":
        if session.xcoding_msg is tls.MessageId.CERTIFICATE:
            return cls(
                status_type=decoder.enum(width=1, cls=tls.StatusType),
                response=decoder.octets(width=3, min_val=1),
            )

        return cls()

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(self.status_type, width=1)
        self.request._encode(encoder, session)

    def __post_init__(self, responder_ids, extensions):
        if responder_ids or extensions:
            request = OCSPStatusRequest(
                responder_id_list=responder_ids or [],
                request_extensions=extensions or b"",
            )

        else:
            request = OCSPStatusRequest()

        self.request = request


@dataclass
class CertificateStatusRequestItemV2(object):
    """Structure part of status_request_v2 extension"""

    status_type: Union[tls.StatusType, int] = tls.StatusType.OCSP
    """The type of the status request. Defaults to :obj:`tlsamte.tls.StatusType.OCSP`
    """

    request: OCSPStatusRequest = field(default_factory=OCSPStatusRequest)
    """The :obj:`OCSPStatusRequest` object.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "CertificateStatusRequestItemV2":
        status_type = decoder.enum(width=1, cls=tls.StatusType)
        request_length = decoder.uint16()
        req_decoder = pdu.Decoder(decoder.octets(fixed_length=request_length))
        request = OCSPStatusRequest._decode(req_decoder, session)
        if req_decoder.get_length():
            raise tls.ServerMalfunction(tls.ServerIssue.LENGTH_TOO_LONG)

        return cls(status_type=status_type, request=request)

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(self.status_type, width=1)
        req_encoder = pdu.Encoder()
        self.request._encode(req_encoder, session)
        req_encoder.add_own_length(width=2)
        encoder.append_encoder(req_encoder)


@dataclass
class StatusRequestV2(Extension):
    """Represents the status_requestv2 extension.

    E.g.,

    >>> StatusRequestV2(status_type=tls.StatusType.OCSP_MULTI)
    StatusRequestV2(certificate_status_req_list=[CertificateStatusRequestItemV2(status_type=<StatusType.OCSP_MULTI: 2>, request=OCSPStatusRequest(responder_id_list=[], request_extensions=b''))])
    """  # noqa

    extension_id: ClassVar = tls.Extension.STATUS_REQUEST_V2
    """:obj:`tlsmate.tls.Extension.STATUS_REQUEST_V2`
    """

    status_type: InitVar[Any] = None
    """The type of the status request. Defaults to :obj:`tlsamte.tls.StatusType.OCSP`
    """

    responder_ids: InitVar[Any] = None
    """Only used during instantiation to build an OCSPStatusRequest.
    """

    extensions: InitVar[Any] = None
    """Only used during instantiation to build an OCSPStatusRequest.
    """

    certificate_status_req_list: List[CertificateStatusRequestItemV2] = field(
        default_factory=list
    )
    """The list of :obj:`CertificateStatusRequestItemV2` objects.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "StatusRequestV2":
        if not decoder.get_length():
            return cls(
                certificate_status_req_list=[
                    CertificateStatusRequestItemV2(status_type=tls.StatusType.NONE)
                ]
            )

        return cls(
            certificate_status_req_list=[
                CertificateStatusRequestItemV2._decode(decoder, session)
                for _ in decoder.loop_length_field(width=2)
            ]
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        for status_req in self.certificate_status_req_list:
            status_req._encode(encoder, session)
        encoder.add_own_length(width=2)

    def add_request(
        self,
        status_type: tls.StatusType,
        responder_ids: Optional[List[bytes]] = None,
        extensions: Optional[bytes] = None,
    ):
        if responder_ids is None:
            responder_ids = []

        if extensions is None:
            extensions = b""

        self.certificate_status_req_list.append(
            CertificateStatusRequestItemV2(
                status_type=status_type,
                request=OCSPStatusRequest(
                    responder_id_list=responder_ids, request_extensions=extensions
                ),
            )
        )

    def __post_init__(self, status_type, responder_ids, extensions):
        if status_type or responder_ids is not None or extensions is not None:
            self.add_request(
                status_type=status_type or tls.StatusType.OCSP,
                responder_ids=responder_ids,
                extensions=extensions,
            )


@dataclass
class SupportedVersions(Extension):
    """Represents the SupportedVersion extension.

    E.g.,

    >>> SupportedVersions(versions=[tls.Version.TLS13, tls.Version.TLS12])
    SupportedVersions(versions=[<Version.TLS13: 772>, <Version.TLS12: 771>], selected_version=None)

    Arbitrary versions can be specified by integer values:

    >>> SupportedVersions(versions=[775, tls.Version.TLS12])
    SupportedVersions(versions=[775, <Version.TLS12: 771>], selected_version=None)
    """  # noqa

    extension_id: ClassVar = tls.Extension.SUPPORTED_VERSIONS
    """:obj:`tlsmate.tls.Extension.SUPPORTED_VERSIONS`
    """

    versions: Optional[List[tls.Version]] = None
    """Only used in ClientHello messages, The list of supported versions by the client.
    """

    selected_version: Optional[tls.Version] = None
    """Only used in ServerHello/HelloRetryRequest messages, the selected version by
    the server.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "SupportedVersions":
        if session.xcoding_msg is tls.MessageId.CLIENT_HELLO:
            return cls(
                versions=[
                    decoder.enum(width=2, cls=tls.Version)
                    for _ in decoder.loop_length_field(width=1, min_val=2, max_val=254)
                ]
            )

        else:
            return cls(selected_version=decoder.enum(width=2, cls=tls.Version))

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        if session.xcoding_msg is tls.MessageId.CLIENT_HELLO:
            encoder.list_length(self.versions, element_width=2, width=1)
            for version in self.versions:
                encoder.enum(version, width=2)

        else:
            encoder.enum(self.selected_version, width=2)


@dataclass
class Cookie(Extension):
    """Represents the Cookie extension.

    E.g.,

    >>> Cookie(cookie=b"deadbeef")
    Cookie(cookie=b'deadbeef')
    """

    extension_id: ClassVar = tls.Extension.COOKIE
    """:obj:`tlsmate.tls.Extension.COOKIE`
    """

    cookie: bytes = b" "
    """The opaque cookie value as an octet string.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "Cookie":
        return cls(cookie=decoder.octets(width=2, min_val=1))

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.octets(self.cookie, width=2)


@dataclass
class KeyShareEntry(object):
    """Stucture part of KeyShare"""

    group: Union[tls.SupportedGroups, int] = 0
    """The named group for this entry. Arbitrary integer values are supported.
    """

    key_exchange: Optional[bytes] = None
    """The public key to be exchanged.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "KeyShareEntry":
        return cls(
            group=decoder.enum(width=2, cls=tls.SupportedGroups),
            key_exchange=decoder.octets(width=2, min_val=1),
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(self.group, width=2)
        encoder.octets(self.key_exchange, width=2)


@dataclass
class KeyShare(Extension):
    """Represents the KeyShare extension.

    To setup this extension for a ClientHello message it is sufficient to just
    provide the list of supported named groups. Before serialization, tlsmate
    will generate the key shares and update this extension accordingly.

    The user must ensure that the key share are a subset of the supported groups
    extension, and that the same sequence for the in both extensions is setup.

    Example to setup this extensions for a ClientHello:

    >>> KeyShare(groups=[tls.SupportedGroups.SECP521R1, tls.SupportedGroups.X25519])
    KeyShare(client_shares=[KeyShareEntry(group=<SupportedGroups.SECP521R1: 25>, key_exchange=None), KeyShareEntry(group=<SupportedGroups.X25519: 29>, key_exchange=None)], server_share=None, selected_group=None)
    """  # noqa

    extension_id: ClassVar = tls.Extension.KEY_SHARE
    """:obj:`tlsmate.tls.Extension.KEY_SHARE`
    """

    groups: InitVar[List[Union[tls.SupportedGroups, int]]] = None
    """Only used during initialization, the list of supported groups.

    Arbitrary integer values may be specified.
    """

    client_shares: Optional[List[KeyShareEntry]] = None
    """Only relevant in ClientHello messages, a list of :obj:`KeyShareEntry` objects.
    """
    server_share: Optional[KeyShareEntry] = None
    """Only relevant in ServerHello messages, the server's selected key share.
    """

    selected_group: Optional[tls.SupportedGroups] = None
    """Only relevant in HelloRetryRequest messages, the named group selected by the
    server.
    """

    @property
    def key_shares(self):
        """For backward compatibility only. May be removed in the future."""
        return [self.server_share]

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "KeyShare":
        if session.xcoding_msg is tls.MessageId.CLIENT_HELLO:
            return cls(
                client_shares=[
                    KeyShareEntry._decode(decoder, session)
                    for _ in decoder.loop_length_field(width=2)
                ]
            )

        elif session.xcoding_msg is tls.MessageId.SERVER_HELLO:
            return cls(server_share=KeyShareEntry._decode(decoder, session))

        else:
            return cls(selected_group=decoder.enum(width=2, cls=tls.SupportedGroups))

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        for share in self.client_shares:
            share._encode(encoder, session)

        encoder.add_own_length(width=2)

    def __post_init__(self, groups):
        if groups is not None:
            self.client_shares = [KeyShareEntry(group=group) for group in groups]


@dataclass
class PskIdentity(object):
    """Stucture part of PreSharedKey"""

    identity: bytes = b" "
    """The identity of the PSK, i.e., the ticket.
    """

    obfuscated_ticket_age: int = 0
    """An integer representing the obfuscated ticket age.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "PskIdentity":
        return cls(
            identity=decoder.octets(width=2, min_val=1),
            obfuscated_ticket_age=decoder.uint32(),
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.octets(self.identity, width=2)
        encoder.uint32(self.obfuscated_ticket_age)


@dataclass
class PreSharedKey(Extension):
    """Represents the PreSharedKey extension for TLS1.3

    For a ClientHello, this extension must be setup with binders of the correct
    length, but the content of the binders does not matter. Note: the length of each
    binder corresponds with the length of the MAC for the given PSK.

    The binders will be updated by tlsmate after the message has been serialized
    til the start of the binders, but before the message is sent.

    Note: RFC8446 states, that this must be the last extension in the
    ClientHello, and tlsmate will conform to this when setting up the
    ClientHello on its own. But nothing prevents a test case to add additional
    extensions after the preshared key extension. In this case those extensions
    will not be considered for the message digest of the ClientHello.
    """

    extension_id: ClassVar = tls.Extension.PRE_SHARED_KEY
    """:obj:`tlsmate.tls.Extension.PRE_SHARED_KEY`
    """

    # ClientHello only
    identities: Optional[List[PskIdentity]] = None
    """ClientHello only, the list of :obj:`PskIdentity` objects.
    """

    binders: Optional[List[bytes]] = None
    """ClientHello only, the binders for the PSKs. Each binder corresponds to an
    entry in `identities`.
    """

    # ServerHello only
    selected_identity: Optional[int] = None
    """ServerHello only, the index of the selected PSKs offered by the client.
    """

    binders_length: int = field(init=False, repr=False)

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "PreSharedKey":
        # TODO: implement server side
        return cls(selected_identity=decoder.uint16())

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        for identity in self.identities:
            identity._encode(encoder, session)

        encoder.add_own_length(width=2)
        binder_encoder = pdu.Encoder()
        for binder in self.binders:
            binder_encoder.octets(binder, width=1)

        binder_encoder.add_own_length(width=2)
        self.binders_length = binder_encoder.get_length()
        encoder.append_encoder(binder_encoder)

    def encode_binders(self):
        encoder = pdu.Encoder()
        for binder in self.binders:
            encoder.octets(binder, width=1)

        encoder.add_own_length(width=2)
        return encoder.get_octet_string()


@dataclass
class PskKeyExchangeMode(Extension):
    """Represents the psk_key_exchange_mode extension.

    E.g.,

    >>> PskKeyExchangeMode(ke_modes=[tls.PskKeyExchangeMode.PSK_DHE_KE, tls.PskKeyExchangeMode.PSK_KE])
    PskKeyExchangeMode(ke_modes=[<PskKeyExchangeMode.PSK_DHE_KE: 1>, <PskKeyExchangeMode.PSK_KE: 0>])
    """  # noqa

    extension_id: ClassVar = tls.Extension.PSK_KEY_EXCHANGE_MODES
    """:obj:`tlsmate.tls.Extension.PSK_KEY_EXCHANGE_MODES`
    """

    modes: InitVar[Any] = None
    """This field is only applicable when instantiating the class.

    It is a synonym for ke_modes. It is here for backward compatibility
    only and will be removed in the future.
    """

    ke_modes: List[tls.PskKeyExchangeMode] = field(default_factory=list)
    """The list of the PSK key exchange modes to offer to the server.
    """

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.list_length(self.ke_modes, element_width=1, width=1)
        for mode in self.ke_modes:
            encoder.enum(mode, width=1)

    def __post_init__(self, modes):
        if modes is not None:
            self.ke_modes = modes


@dataclass
class EarlyData(Extension):
    """Represents the EarlyData extension.

    E.g.,

    >>> EarlyData(max_early_data_size=200)
    EarlyData(max_early_data_size=200)
    """

    extension_id: ClassVar = tls.Extension.EARLY_DATA
    """:obj:`tlsmate.tls.Extension.EARLY_DATA`
    """

    max_early_data_size: Optional[int] = None
    """The maximum number of bytes used for the early data.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "EarlyData":
        if session.xcoding_msg is tls.MessageId.NEW_SESSION_TICKET:
            return cls(max_early_data_size=decoder.uint32())

        else:
            return cls()

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        if session.xcoding_msg is tls.MessageId.NEW_SESSION_TICKET:
            encoder.uint32(self.max_early_data_size)


@dataclass
class PostHandshakeAuth(Extension):
    """Represents the PostHandshakeAuth extension.

    E.g.,

    >>> PostHandshakeAuth()
    PostHandshakeAuth()
    """

    extension_id: ClassVar = tls.Extension.POST_HANDSHAKE_AUTH
    """:obj:`tlsmate.tls.Extension.POST_HANDSHAKE_AUTH`
    """


@dataclass
class ApplLayerProtNeg(Extension):
    """Represents the Application Layer Protocol Negotiation extension.

    E.g.,

    >>> ApplLayerProtNeg(protocol_name_list=[tls.Alpn.HTTP2, tls.Alpn.SPDY2, b"myapp"])
    ApplLayerProtNeg(protocol_name_list=[<Alpn.HTTP2: b'h2'>, <Alpn.SPDY2: b'spdy/2'>, b'myapp'])
    """  # noqa

    extension_id: ClassVar = tls.Extension.APPLICATION_LAYER_PROTOCOL_NEGOTIATION
    """:obj:`tlsmate.tls.Extension.APPLICATION_LAYER_PROTOCOL_NEGOTIATION`
    """

    # for backward compatibility
    protocols: InitVar = None
    """This field is only applicable when instantiating the class.

    It is a synonym for protocol_name_list. It is here for backward compatibility
    only and will be removed in the future.
    """

    protocol_name_list: List[Union[tls.Alpn, bytes]] = field(
        default_factory=lambda: [b" "]
    )
    """The list of protocol names. The names can be given as :obj:`tls.Alpn` objects,
    as strings or as bytes. When decoding, the names are represented as
    :obj:`tls.Alpn` objects, or - if unknown - as bytes.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ApplLayerProtNeg":
        protocols = []
        for _ in decoder.loop_length_field(width=2, min_val=2):
            oct_val = decoder.octets(width=1, min_val=1)
            protocols.append(tls.Alpn.val2enum(oct_val) or oct_val)

        return cls(protocol_name_list=protocols)

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        for name in self.protocol_name_list:
            if isinstance(name, tls.Alpn):
                name = name.value

            if isinstance(name, str):
                name = name.encode()

            encoder.octets(name, width=1)
        encoder.add_own_length(width=2)

    def __post_init__(self, protocols):
        if protocols is not None:
            self.protocol_name_list = protocols


# Map the extensions id to the corresponding class.
deserialization_map: Mapping[tls.Extension, Type[Extension]] = {
    tls.Extension.SERVER_NAME: ServerNameIndication,
    # tls.Extension.MAX_FRAGMENT_LENGTH = 1
    # tls.Extension.CLIENT_CERTIFICATE_URL = 2
    # tls.Extension.TRUSTED_CA_KEYS = 3
    # tls.Extension.TRUNCATED_HMAC = 4
    tls.Extension.STATUS_REQUEST: StatusRequest,
    # tls.Extension.USER_MAPPING = 6
    # tls.Extension.CLIENT_AUTHZ = 7
    # tls.Extension.SERVER_AUTHZ = 8
    # tls.Extension.CERT_TYPE = 9
    tls.Extension.SUPPORTED_GROUPS: SupportedGroups,
    tls.Extension.EC_POINT_FORMATS: EcPointFormats,
    # tls.Extension.SRP = 12
    tls.Extension.SIGNATURE_ALGORITHMS: SignatureAlgorithms,
    # tls.Extension.USE_SRTP = 14
    tls.Extension.HEARTBEAT: Heartbeat,
    tls.Extension.APPLICATION_LAYER_PROTOCOL_NEGOTIATION: ApplLayerProtNeg,
    tls.Extension.STATUS_REQUEST_V2: StatusRequestV2,
    # tls.Extension.SIGNED_CERTIFICATE_TIMESTAMP = 18
    # tls.Extension.CLIENT_CERTIFICATE_TYPE = 19
    # tls.Extension.SERVER_CERTIFICATE_TYPE = 20
    # tls.Extension.PADDING = 21
    tls.Extension.ENCRYPT_THEN_MAC: EncryptThenMac,
    tls.Extension.EXTENDED_MASTER_SECRET: ExtendedMasterSecret,
    # tls.Extension.TOKEN_BINDING = 24
    # tls.Extension.CACHED_INFO = 25
    # tls.Extension.TLS_LTS = 26
    # tls.Extension.COMPRESS_CERTIFICATE = 27
    # tls.Extension.RECORD_SIZE_LIMIT = 28
    # tls.Extension.PWD_PROTECT = 29
    # tls.Extension.PWD_CLEAR = 30
    # tls.Extension.PASSWORD_SALT = 31
    # tls.Extension.TICKET_PINNING = 32
    # tls.Extension.TLS_CERT_WITH_EXTERN_PSK = 33
    # tls.Extension.DELEGATED_CREDENTIALS = 34
    tls.Extension.SESSION_TICKET: SessionTicket,
    # tls.Extension.SUPPORTED_EKT_CIPHERS = 39
    tls.Extension.PRE_SHARED_KEY: PreSharedKey,
    tls.Extension.EARLY_DATA: EarlyData,
    tls.Extension.SUPPORTED_VERSIONS: SupportedVersions,
    tls.Extension.COOKIE: Cookie,
    # tls.Extension.PSK_KEY_EXCHANGE_MODES = 45
    tls.Extension.CERTIFICATE_AUTHORITIES: CertificateAuthorities,
    # tls.Extension.OID_FILTERS = 48
    tls.Extension.POST_HANDSHAKE_AUTH: PostHandshakeAuth,
    # tls.Extension.SIGNATURE_ALGORITHMS_CERT = 50
    tls.Extension.KEY_SHARE: KeyShare,
    # tls.Extension.TRANSPARENCY_INFO = 52
    # tls.Extension.CONNECTION_ID = 53
    # tls.Extension.EXTERNAL_ID_HASH = 55
    # tls.Extension.EXTERNAL_SESSION_ID = 56
    tls.Extension.RENEGOTIATION_INFO: RenegotiationInfo,
}


def log_extensions(extensions: List[Extension]) -> None:

    for extension in extensions:
        ext_id = extension.extension_id
        logging.debug(f"extension {ext_id.value} {ext_id}")


def get_extension(
    extensions: List[Extension], ext_id: tls.Extension
) -> Optional[Extension]:

    if extensions is not None:
        for extension in extensions:
            if extension.extension_id == ext_id:
                return extension

    return None


# For backward compatibility, will be removed in the future.
ExtServerNameIndication = ServerNameIndication
ExtStatusRequest = StatusRequest
ExtSupportedGroups = SupportedGroups
ExtEcPointFormats = EcPointFormats
ExtSignatureAlgorithms = SignatureAlgorithms
ExtHeartbeat = Heartbeat
ExtApplLayerProtNeg = ApplLayerProtNeg
ExtStatusRequestV2 = StatusRequestV2
ExtEncryptThenMac = EncryptThenMac
ExtExtendedMasterSecret = ExtendedMasterSecret
ExtSessionTicket = SessionTicket
ExtPreSharedKey = PreSharedKey
ExtEarlyData = EarlyData
ExtSupportedVersions = SupportedVersions
ExtCookie = Cookie
ExtCertificateAuthorities = CertificateAuthorities
ExtPostHandshakeAuth = PostHandshakeAuth
ExtKeyShare = KeyShare
ExtRenegotiationInfo = RenegotiationInfo
ExtPskKeyExchangeMode = PskKeyExchangeMode
ExtUnknownExtension = UnknownExtension
