# -*- coding: utf-8 -*-
"""Module providing classes for each TLS message.
"""
# import basic stuff
import abc
import os
import time

from dataclasses import dataclass, field
from typing import (
    Any as AnyType,
    Callable,
    ClassVar,
    List,
    Mapping,
    Optional,
    Tuple,
    Type,
    Union,
)

# import own stuff
import tlsmate.cert_chain as cert_chain
import tlsmate.client_auth as client_auth
import tlsmate.client_state as client_state
import tlsmate.ext as ext
import tlsmate.pdu as pdu
import tlsmate.recorder as rec
import tlsmate.structs as structs
import tlsmate.tls as tls
import tlsmate.utils as utils

# import other stuff


def _get_version(msg):
    """Get the highest version from a hello message.

    Arguments:
        msg (:obj:`HandshakeMessage`): the message

    Returns:
        :class:`tls.Version`: The highest version
    """

    supported_versions = msg.get_extension(tls.Extension.SUPPORTED_VERSIONS)
    if supported_versions is not None:
        return supported_versions.selected_version

    return msg.version


class TlsMessage(metaclass=abc.ABCMeta):
    """Abstract base class for TLS messages"""

    content_type: ClassVar[tls.ContentType]
    """The content type of the message.
    """

    msg_id: ClassVar
    """Unique identifier of the message across all content types
    """

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        """Encodes the message"""
        pass  # TODO: Make class abstract when server side is implemented.

    def serialize(self, session: client_state.SessionState) -> bytes:
        """Encodes the message and returns the encoded octet string.

        Note: a session object is required, as encoding or decoding a TLS message
        without such a context is not possible in every case.

        Arguments:
            session: the :obj:`tlsmate.client_state.SessionState` object which
                holds various information required to encode some messages/extensions.

        Returns:
            the encoded octet string, starting with the content type
        """
        session.xcoding_msg = self.msg_id
        encoder = pdu.Encoder()
        self._encode(encoder, session)
        return encoder.get_octet_string()


class Any(object):
    """Class to represent any message to wait for in a test case."""

    pass


class Timeout(object):
    """Class to allow waiting for a timeout."""

    msg_id = "Timeout"


class HandshakeMessage(TlsMessage):
    """A base class for all handshake messages."""

    content_type: ClassVar = tls.ContentType.HANDSHAKE
    """:obj:`tlsmate.tls.ContentType.HANDSHAKE`
    """

    msg_type: ClassVar[tls.HandshakeType]
    """The type of the handshake message.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "HandshakeMessage":

        msg_type = decoder.enum(width=1, cls=tls.HandshakeType)
        decoder.skip(3)  # length already read by defragmenter
        cls = _hs_deserialization_map.get(msg_type)
        session.xcoding_msg = cls.msg_id
        if cls is None:
            raise tls.ServerMalfunction(
                tls.ServerIssue.UNKNOWN_HANDSHAKE_MSG, value=int(msg_type)
            )

        try:
            message = cls._decode(decoder, session)

        except tls.ServerMalfunction as exc:
            # enrich the exception with the message id
            exc.malfunction.message = cls.msg_id
            raise exc

        return message

    def serialize(self, session: client_state.SessionState) -> bytes:
        """Serialize the handshake message into an octet string.

        Arguments:
            session: the :obj:`tlsmate.client_state.SessionState` object which
                holds various information required to encode some messages/extensions.

        Returns:
            the encoded message, starting with the content type
        """

        session.xcoding_msg = self.msg_id
        encoder = pdu.Encoder()
        encoder.enum(self.msg_type, width=1)
        body_encoder = pdu.Encoder()
        self._encode(body_encoder, session)
        body_encoder.add_own_length(width=3)
        encoder.append_encoder(body_encoder)
        return encoder.get_octet_string()


@dataclass
class HelloRequest(HandshakeMessage):
    """This class represents a HelloRequest message."""

    msg_type: ClassVar = tls.HandshakeType.HELLO_REQUEST
    """:obj:`tlsmate.tls.HandshakeType.HELLO_REQUEST`
    """

    msg_id: ClassVar = tls.MessageId.HELLO_REQUEST
    """:obj:`tlsmate.tls.MessageId.HELLO_REQUEST`
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "HelloRequest":
        return cls()


@dataclass
class ClientHello(HandshakeMessage):
    """This class represents a ClientHello message."""

    msg_id: ClassVar = tls.MessageId.CLIENT_HELLO
    """:obj:`tlsmate.tls.MessageId.CLIENT_HELLO`
    """
    msg_type: ClassVar = tls.HandshakeType.CLIENT_HELLO
    """:obj:`tlsmate.tls.HandshakeType.CLIENT_HELLO`
    """

    version: tls.Version = tls.Version.TLS12
    """The version the client offers to the server.
    """

    random: bytes = field(default_factory=utils.get_random_value)
    """The random value"""

    session_id: bytes = b""
    """The session id offered by the client"""

    cipher_suites: List[Union[tls.CipherSuite, int]] = field(default_factory=list)
    """The list of cipher suites to offer to the server. May contain integer values.
    """

    compression_methods: List[Union[tls.CompressionMethod, int]] = field(
        default_factory=lambda: [tls.CompressionMethod.NULL]
    )
    """The list of compression methods offered to the server. May contain integer
    values.
    """

    extensions: List[ext.Extension] = field(default_factory=list)
    """The list of extensions (:obj:`tlsmate.ext.Extension`) offered to the server.
    """

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(self.version, width=2)
        encoder.octets(self.random)
        encoder.octets(self.session_id, width=1)

        encoder.list_length(self.cipher_suites, element_width=2, width=2)
        for cs in self.cipher_suites:
            encoder.enum(cs, width=2)

        encoder.list_length(self.compression_methods, element_width=1, width=1)
        for compr in self.compression_methods:
            encoder.enum(compr, width=1)

        if self.version >= tls.Version.TLS10:
            ext.encode_extensions(self.extensions, encoder, session)

    def get_extension(self, ext_id: tls.Extension) -> Optional[ext.Extension]:
        """Method to extract a specific extension.

        Arguments:
            ext_id: The extension id to look for.

        Returns:
            The extension object or None if not present.
        """

        return ext.get_extension(self.extensions, ext_id)

    def get_version(self) -> tls.Version:
        """Get the highest TLS version from the message.

        Takes the version parameter into account as well as the extension
        SUPPORTED_VERSIONS (if present).

        Returns:
            The highest TLS version offered.
        """

        return _get_version(self)

    def replace_extension(self, new_ext: ext.Extension) -> None:
        """Replaces the extension by the existing one or append it at the end.

        Ensure, that the pre-shared key extensions stays the last one if present.

        Arguments:
            new_ext: the extension to replace.
        """

        for idx, extension in enumerate(self.extensions):
            if extension.extension_id is new_ext.extension_id:
                self.extensions[idx] = new_ext
                return

        if (
            self.extensions
            and self.extensions[-1].extension_id is tls.Extension.PRE_SHARED_KEY
        ):
            self.extensions.insert(-1, new_ext)

        else:
            self.extensions.append(new_ext)


@dataclass
class ServerHello(HandshakeMessage):
    """This class represents a ServerHello message."""

    msg_id: ClassVar = tls.MessageId.SERVER_HELLO
    """:obj:`tlsmate.tls.MessageId.SERVER_HELLO`
    """
    msg_type: ClassVar = tls.HandshakeType.SERVER_HELLO
    """:obj:`tlsmate.tls.HandshakeType.SERVER_HELLO`
    """

    HELLO_RETRY_REQ_RAND = bytes.fromhex(
        "CF 21 AD 74 E5 9A 61 11 BE 1D 8C 02 1E 65 B8 91 "
        "C2 A2 11 16 7A BB 8C 5E 07 9E 09 E2 C8 A8 33 9C "
    )

    version: tls.Version = tls.Version.TLS12
    """The version selected by the server.
    """

    random: bytes = b""
    """The random value"""

    session_id: bytes = b""
    """The session id returned by the server"""

    cipher_suite: tls.CipherSuite = tls.CipherSuite.TLS_RSA_WITH_AES_128_CBC_SHA256
    """The selected cipher suite by the server.
    """

    compression_method: tls.CompressionMethod = tls.CompressionMethod.NULL
    """The selected compression method by the server.
    """

    extensions: List[ext.Extension] = field(default_factory=list)
    """The list of extensions (:obj:`tlsmate.ext.Extension`) returned by the server.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> Union["ServerHello", "HelloRetryRequest"]:
        version = decoder.enum(width=2, cls=tls.Version)
        random = decoder.octets(fixed_length=32)
        if random == cls.HELLO_RETRY_REQ_RAND:
            cls = HelloRetryRequest
            session.xcoding_msg = tls.MessageId.HELLO_RETRY_REQUEST

        return cls(
            version=version,
            random=random,
            session_id=decoder.octets(width=1, max_val=32),
            cipher_suite=decoder.enum(width=2, cls=tls.CipherSuite),
            compression_method=decoder.enum(width=1, cls=tls.CompressionMethod),
            extensions=ext.decode_extensions(decoder, session),
        )

    def get_extension(self, ext_id: tls.Extension) -> Optional[ext.Extension]:
        """Get an extension from the message

        Arguments:
            ext_id: The extensions to look for

        Returns:
            The extension or None if not present.
        """

        return ext.get_extension(self.extensions, ext_id)

    def get_version(self) -> tls.Version:
        """Get the negotiated TLS version from the message.

        Takes the version parameter into account as well as the extension
        SUPPORTED_VERSIONS (if present).

        Returns:
            The negotiated TLS version.
        """

        return _get_version(self)


class HelloRetryRequest(ServerHello):
    """This class represents HelloRetryRequest message.

    This message has exactly the same attributes as a ServerHello message,
    with the only difference that the random attribute has a fixed value.
    """

    msg_id: ClassVar = tls.MessageId.HELLO_RETRY_REQUEST
    """:obj:`tlsmate.tls.MessageId.HELLO_RETRY_REQUEST`
    """


@dataclass
class CertificateEntry(object):
    """Sub class for certificate message"""

    cert_data: bytes = b" "
    """The certificate in ASN.1 format.
    """

    extensions: List[ext.Extension] = field(default_factory=list)
    """The list of extensions related to this certificate.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "CertificateEntry":
        if session.version is tls.Version.TLS13:
            return cls(
                cert_data=decoder.octets(width=3, min_val=1),
                extensions=ext.decode_extensions(decoder, session),
            )

        else:
            return cls(cert_data=decoder.octets(width=3, min_val=1),)

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.octets(self.cert_data, width=3)
        if session.version is tls.Version.TLS13:
            ext.encode_extensions(self.extensions, encoder, session)


@dataclass
class Certificate(HandshakeMessage):
    """This class represents a Certificate message."""

    msg_id: ClassVar = tls.MessageId.CERTIFICATE
    """:obj:`tlsmate.tls.MessageId.CERTIFICATE`
    """
    msg_type: ClassVar = tls.HandshakeType.CERTIFICATE
    """:obj:`tlsmate.tls.HandshakeType.CERTIFICATE`
    """

    certificate_request_context: Optional[bytes] = None
    """The certificate context for requests.
    """

    certificate_list: List[CertificateEntry] = field(default_factory=list)
    """A list of :obj:`CertificateEntry` objects.
    """

    @property
    def chain(self):
        """For backward compatibility. Might be removed in the future.
        """
        if self._chain is None:
            self._chain = self.get_cert_chain()

        return self._chain

    def __post_init__(self) -> None:
        self._chain = None

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "Certificate":
        if session.version is tls.Version.TLS13:
            return cls(
                certificate_request_context=decoder.octets(width=1),
                certificate_list=[
                    CertificateEntry._decode(decoder, session)
                    for _ in decoder.loop_length_field(width=3)
                ],
            )

        else:
            return cls(
                certificate_list=[
                    CertificateEntry._decode(decoder, session)
                    for _ in decoder.loop_length_field(width=3)
                ],
            )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        if self.certificate_request_context is not None:
            encoder.octets(self.certificate_request_context, width=1)

        list_encoder = pdu.Encoder()
        for cert in self.certificate_list:
            cert._encode(list_encoder, session)

        list_encoder.add_own_length(width=3)
        encoder.append_encoder(list_encoder)

    def get_cert_chain(self) -> cert_chain.CertChain:
        chain = cert_chain.CertChain()
        for cert in self.certificate_list:
            chain.append_bin_cert(cert.cert_data)

        return chain


@dataclass
class CertificateVerify(HandshakeMessage):
    """This class represents a CertificateVerify message."""

    msg_id: ClassVar = tls.MessageId.CERTIFICATE_VERIFY
    """:obj:`tlsmate.tls.MessageId.CERTIFICATE_VERIFY`
    """
    msg_type: ClassVar = tls.HandshakeType.CERTIFICATE_VERIFY
    """:obj:`tlsmate.tls.HandshakeType.CERTIFICATE_VERIFY`
    """

    algorithm: tls.SignatureScheme = tls.SignatureScheme.RSA_PKCS1_SHA256
    """The signature algorithm used for the signature.
    """

    signature: bytes = b""
    """The signature.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "CertificateVerify":
        return cls(
            algorithm=decoder.enum(width=2, cls=tls.SignatureScheme),
            signature=decoder.octets(width=2),
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(self.algorithm, width=2)
        encoder.octets(self.signature, width=2)


@dataclass
class ECCurve(object):
    """Describes an elliptic curve"""

    a: bytes = b" "
    """The a-coefficient of the curve.
    """

    b: bytes = b" "
    """The b-coefficient of the curve.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ECCurve":
        return cls(
            a=decoder.octets(width=1, min_val=1), b=decoder.octets(width=1, min_val=1),
        )


@dataclass
class ECPoint(object):
    """Describes a point on an elleiptic curve."""

    point: bytes = b" "
    """The point represented as an octet string.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ECPoint":
        return cls(point=decoder.octets(width=1, min_val=1))


@dataclass
class ECParameters(object):
    """Defines the parameters for an elliptic curve."""

    curve_type: tls.EcCurveType = tls.EcCurveType.NAMED_CURVE
    """The type of the curve.
    """

    m: Optional[int] = None
    """The degree of the characteristic-2 field.
    """

    basis: Optional[tls.ECBasisType] = None
    """The basis type.
    """

    k: Optional[bytes] = None
    """The exponent k for the trinomial basis representation.
    """

    k1: Optional[bytes] = None
    """The exponent k1 for the pentanomial representation.
    """

    k2: Optional[bytes] = None
    """The exponent k2 for the pentanomial representation.
    """

    k3: Optional[bytes] = None
    """The exponent k3 for the pentanomial representation.
    """

    prime_p: Optional[bytes] = None
    """The odd prime defining the field Fp.
    """

    curve: Optional[ECCurve] = None
    """Specifies the coefficients a and b of the elliptic curve.
    """

    base: Optional[ECPoint] = None
    """Specifies the base point G on the elliptic curve.
    """

    order: Optional[bytes] = None
    """Specifies the order n of the base point.
    """

    cofactor: Optional[bytes] = None
    """Specifies the cofactor.
    """

    named_curve: Optional[tls.SupportedGroups] = None
    """Specifies the name of the curve.
    """

    # for backward compatibility only.
    @property
    def curve_name(self):
        """A synonym for named_curve. Will be removed in the future.
        """
        return self.named_curve

    @curve_name.setter
    def curve_name(self, value):
        self.named_curve = value

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ECParameters":

        curve_type = decoder.enum(width=1, cls=tls.EcCurveType)
        if curve_type is tls.EcCurveType.NAMED_CURVE:
            return cls(
                curve_type=curve_type,
                named_curve=decoder.enum(width=2, cls=tls.SupportedGroups),
            )

        elif curve_type is tls.EcCurveType.EXPLICIT_PRIME:
            return cls(
                curve_type=curve_type,
                prime_p=decoder.octets(width=1, min_val=1),
                curve=ECCurve._decode(decoder, session),
                order=decoder.octets(width=1, min_val=1),
                cofactor=decoder.octets(width=1, min_val=1),
            )

        elif curve_type is tls.EcCurveType.EXPLICIT_CHAR2:
            if True:  # fictional basis is EC_BASIS_TRINOMIAL
                return cls(
                    curve_type=curve_type,
                    basis=tls.ECBasisType.EC_BASIS_TRINOMIAL,
                    m=decoder.uint16(),
                    k=decoder.octets(width=1, min_val=1),
                )

            else:  # fictional basis is EC_BASIS_PENTANOMIAL
                return cls(
                    curve_type=curve_type,
                    basis=tls.ECBasisType.EC_BASIS_PENTANOMIAL,
                    m=decoder.uit16(),
                    k1=decoder.octets(width=1, min_val=1),
                    k2=decoder.octets(width=1, min_val=1),
                    k3=decoder.octets(width=1, min_val=1),
                )

        raise tls.ServerMalfunction(tls.ServerIssue.INCOMPATIBLE_KEY_EXCHANGE)


@dataclass
class ServerECDHParams(object):
    """Defines the server parameters for an elliptic cureve DH key exchange."""

    sig_scheme: tls.SignatureScheme = field(init=False)
    """For backward compatibility. Might be removed in the future.
    """

    curve_params: ECParameters = field(default_factory=ECParameters)
    """The :obj:`ECParameters` object.
    """

    public: bytes = b""
    """The public ECDH key.
    """

    @property
    def named_curve(self):
        """For backward compatibility. Might be removed in the future.
        """
        return self.curve_params.named_curve

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ServerECDHParams":
        return cls(
            curve_params=ECParameters._decode(decoder, session),
            public=decoder.octets(width=1, min_val=1),
        )

    def set_sig_scheme(self, sig_scheme) -> None:
        """For backward compatibility. Might be removed in the future.
        """
        self.sig_scheme = sig_scheme


@dataclass
class ServerDHParams(object):
    """Defines the server parameters for a DH key exchange."""

    sig_scheme: tls.SignatureScheme = field(init=False)
    """For backward compatibility. Might be removed in the future.
    """

    dh_p: bytes = b""
    """The prime modulus.
    """

    dh_g: bytes = b""
    """The generator.
    """

    dh_Ys: bytes = b""
    """The server's Diffie-Hellman public value.
    """

    @property
    def p_val(self):
        """For backward compatibility. Might be removed in the future.
        """
        return self.dh_p

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ServerDHParams":
        return cls(
            dh_p=decoder.octets(width=2, min_val=1),
            dh_g=decoder.octets(width=2, min_val=1),
            dh_Ys=decoder.octets(width=2, min_val=1),
        )

    def set_sig_scheme(self, sig_scheme) -> None:
        """For backward compatibility. Might be removed in the future.
        """
        self.sig_scheme = sig_scheme


@dataclass
class ServerRSAParams(object):
    """Defines the server parameters for an RSA_EXPORT key exchange."""

    sig_scheme: tls.SignatureScheme = field(init=False)
    """For backward compatibility. Might be removed in the future.
    """

    rsa_modulus: bytes = b""
    """The RSA modulus"""

    rsa_exponent: bytes = b""
    """The RSA exponent"""

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ServerRSAParams":
        return cls(
            rsa_modulus=decoder.octets(width=2, min_val=1),
            rsa_exponent=decoder.octets(width=2, min_val=1),
        )

    def set_sig_scheme(self, sig_scheme) -> None:
        """For backward compatibility. Might be removed in the future.
        """
        self.sig_scheme = sig_scheme


@dataclass
class ServerKeyExchange(HandshakeMessage):
    """This class represents a ServerKeyExchange message.

    The available attributes depend on the key exchange type. If they are not
    applicable, they are set to None.
    """

    msg_id: ClassVar = tls.MessageId.SERVER_KEY_EXCHANGE
    """:obj:`tlsmate.tls.MessageId.SERVER_KEY_EXCHANGE`
    """
    msg_type: ClassVar = tls.HandshakeType.SERVER_KEY_EXCHANGE
    """:obj:`tlsmate.tls.HandshakeType.SERVER_KEY_EXCHANGE`
    """

    params_ecdh: Optional[ServerECDHParams] = None
    """The :obj:`ServerECDHParams` object in case an ECDH key exchange is applicable.
    """

    params_dh: Optional[ServerDHParams] = None
    """The :obj:`ServerDHParams` object in case a DH key exchange is applicable.
    """

    params_rsa: Optional[ServerRSAParams] = None
    """The :obj:`ServerRSAParams` object in case a RSA key exchange is applicable.
    """

    algorithm: Optional[tls.SignatureScheme] = None
    """The signature scheme.
    """

    signature: Optional[bytes] = None
    """The signature.
    """

    signed_octets: Optional[bytes] = field(default=None, repr=False)

    @property
    def ec(self):
        """For backward compatibility only. May be removed in the future.
        """
        return self.params_ecdh

    @property
    def dh(self):
        """For backward compatibility only. May be removed in the future.
        """
        return self.params_dh

    @classmethod
    def _decode_algorithm(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> Optional[tls.SignatureScheme]:
        if session.version is tls.Version.TLS12:
            return decoder.enum(width=2, cls=tls.SignatureScheme)

        return None

    @classmethod
    def _decode_signature(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> Tuple[Optional[tls.SignatureScheme], Optional[bytes]]:
        if (
            session.cs_details.key_algo_struct.key_auth is tls.KeyAuthentication.NONE
            or session.cs_details.key_algo is tls.KeyExchangeAlgorithm.ECDH_ANON
        ):
            return None, None

        return cls._decode_algorithm(decoder, session), decoder.octets(width=2)

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ServerKeyExchange":

        kwargs: Mapping[str, AnyType]
        decoder.set_marker()
        params: Union[ServerECDHParams, ServerDHParams, ServerRSAParams]
        if session.cs_details.key_algo_struct.key_ex_type is tls.KeyExchangeType.ECDH:
            params = ServerECDHParams._decode(decoder, session)
            kwargs = {"params_ecdh": params}

        elif session.cs_details.key_algo_struct.key_ex_type is tls.KeyExchangeType.DH:
            params = ServerDHParams._decode(decoder, session)
            kwargs = {"params_dh": params}

        elif session.cs_details.key_algo is tls.KeyExchangeAlgorithm.RSA_EXPORT:
            params = ServerRSAParams._decode(decoder, session)
            kwargs = {"params_rsa": params}

        else:
            raise tls.ServerMalfunction(tls.ServerIssue.INCOMPATIBLE_KEY_EXCHANGE)

        signed_octets = decoder.from_marker_to_here()
        algorithm, signature = cls._decode_signature(decoder, session)

        # For backward compatibility only. May be removed in the future.
        params.set_sig_scheme(algorithm)
        return cls(
            **kwargs,
            algorithm=algorithm,
            signature=signature,
            signed_octets=signed_octets,
        )


@dataclass
class ServerHelloDone(HandshakeMessage):
    """This class represents a ServerHelloDone message."""

    msg_id: ClassVar = tls.MessageId.SERVER_HELLO_DONE
    """:obj:`tlsmate.tls.MessageId.SERVER_HELLO_DONE`
    """
    msg_type: ClassVar = tls.HandshakeType.SERVER_HELLO_DONE
    """:obj:`tlsmate.tls.HandshakeType.SERVER_HELLO_DONE`
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ServerHelloDone":
        return cls()


@dataclass
class ClientKeyExchange(HandshakeMessage):
    """This class represents a ClientKeyExchange message."""

    msg_id: ClassVar = tls.MessageId.CLIENT_KEY_EXCHANGE
    """:obj:`tlsmate.tls.MessageId.CLIENT_KEY_EXCHANGE`
    """
    msg_type = tls.HandshakeType.CLIENT_KEY_EXCHANGE
    """:obj:`tlsmate.tls.HandshakeType.CLIENT_KEY_EXCHANGE`
    """

    rsa_encrypted_pms: Optional[bytes] = None
    """The RSA encrypted premaster secret, only applicable for RSA key based key
    exchanges.
    """

    dh_public: Optional[bytes] = None
    """The DH public key, only applicable for DH based key exchanges.
    """

    ecdh_public: Optional[bytes] = None
    """The ECDH public key, only applicable for ECDH based key exchanges.
    """

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        if self.rsa_encrypted_pms is not None:
            encoder.octets(
                self.rsa_encrypted_pms,
                width=0 if session.version is tls.Version.SSL30 else 2,
            )

        elif self.dh_public is not None:
            encoder.octets(self.dh_public, width=2)

        elif self.ecdh_public is not None:
            encoder.octets(self.ecdh_public, width=1)


@dataclass
class Finished(HandshakeMessage):
    """This class represents a Finished message."""

    msg_id: ClassVar = tls.MessageId.FINISHED
    """:obj:`tlsmate.tls.MessageId.FINISHED`
    """
    msg_type = tls.HandshakeType.FINISHED
    """:obj:`tlsmate.tls.HandshakeType.FINISHED`
    """

    verify_data: bytes = b""
    """The data to verify that both sides share the same symmetric keys.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "Finished":
        return cls(verify_data=decoder.octets())

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.octets(self.verify_data)


@dataclass
class EndOfEarlyData(HandshakeMessage):
    """This class represents an EndOfEarlyData message."""

    msg_id: ClassVar = tls.MessageId.END_OF_EARLY_DATA
    """:obj:`tlsmate.tls.MessageId.END_OF_EARLY_DATA`
    """
    msg_type = tls.HandshakeType.END_OF_EARLY_DATA
    """:obj:`tlsmate.tls.HandshakeType.END_OF_EARLY_DATA`
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "EndOfEarlyData":
        return cls()

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        pass


@dataclass
class NewSessionTicket(HandshakeMessage):
    """This class represents a NewSessionTicket message."""

    msg_id: ClassVar = tls.MessageId.NEW_SESSION_TICKET
    """:obj:`tlsmate.tls.MessageId.NEW_SESSION_TICKET`
    """
    msg_type = tls.HandshakeType.NEW_SESSION_TICKET
    """:obj:`tlsmate.tls.HandshakeType.NEW_SESSION_TICKET`
    """

    ticket_lifetime: int = 0
    """The lifetime hint for the ticket in seconds.
    """

    ticket_age_add: Optional[int] = None
    """The age_add parameter (only TLS1.3)
    """

    ticket_nonce: Optional[bytes] = None
    """The nonce (only TLS1.3)
    """

    ticket: bytes = b" "
    """The opaque ticket.
    """

    extensions: Optional[List[ext.Extension]] = None
    """The list of :obj:`tlsmate.tls.Extension` objects (only TLS1.3).
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "NewSessionTicket":
        if session.version is tls.Version.TLS13:
            return cls(
                ticket_lifetime=decoder.uint32(),
                ticket_age_add=decoder.uint32(),
                ticket_nonce=decoder.octets(width=1),
                ticket=decoder.octets(width=2, min_val=1),
                extensions=ext.decode_extensions(decoder, session),
            )

        else:
            return cls(
                ticket_lifetime=decoder.uint32(),
                ticket=decoder.octets(width=2, min_val=1),
            )

    def get_extension(self, ext_id: tls.Extension) -> Optional[ext.Extension]:
        """Get an extension from the message

        Arguments:
            ext_id: The extensions to look for

        Returns:
            The extension or None if not present.
        """

        if self.extensions is None:
            return None

        return ext.get_extension(self.extensions, ext_id)


@dataclass
class CertificateRequest(HandshakeMessage):
    """This class represents a CertificateRequest message."""

    msg_id: ClassVar = tls.MessageId.CERTIFICATE_REQUEST
    """:obj:`tlsmate.tls.MessageId.CERTIFICATE_REQUEST`
    """
    msg_type = tls.HandshakeType.CERTIFICATE_REQUEST
    """:obj:`tlsmate.tls.HandshakeType.CERTIFICATE_REQUEST`
    """

    certificate_types: Optional[List[tls.CertType]] = None
    """A list of :obj:`tlsmate.tls.CertType` objects. Only used for TLS1.2 and below.
    """

    supported_signature_algorithms: Optional[List[tls.SignatureScheme]] = None
    """A list of :obj:`tlsmate.tls.SignatureScheme` objects. Only used for
    TLS1.2 and below.
    """

    certificate_authorities: Optional[List[bytes]] = None
    """A list of certificate authorities given as octet strings. Only used for
    TLS1.2 and below.
    """

    certificate_request_context: Optional[bytes] = None
    """An octet string pecifying the request context. Only used for TLS1.3.
    """

    extensions: Optional[List[ext.Extension]] = None
    """A list of :obj:`tlsmate.ext.Extension` objects. Only used for TLS1.3.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "CertificateRequest":
        if session.version is tls.Version.TLS13:
            return cls(
                certificate_request_context=decoder.octets(width=1),
                extensions=ext.decode_extensions(decoder, session),
            )

        else:
            certificate_types = [
                decoder.enum(width=1, cls=tls.CertType)
                for _ in decoder.loop_length_field(width=1, min_val=1)
            ]
            supported_signature_algorithms = None
            if session.version is tls.Version.TLS12:
                supported_signature_algorithms = [
                    decoder.enum(width=2, cls=tls.SignatureScheme)
                    for _ in decoder.loop_length_field(width=2, min_val=2)
                ]

            certificate_authorities = [
                decoder.octets(width=2, min_val=1)
                for _ in decoder.loop_length_field(width=2)
            ]

            return cls(
                certificate_types=certificate_types,
                supported_signature_algorithms=supported_signature_algorithms,
                certificate_authorities=certificate_authorities,
            )

    def get_extension(self, ext_id: tls.Extension) -> Optional[ext.Extension]:
        """Get an extension from the message

        Arguments:
            ext_id: The extensions to look for

        Returns:
            The extension or None if not present.
        """
        if self.extensions is None:
            return None

        return ext.get_extension(self.extensions, ext_id)


@dataclass
class EncryptedExtensions(HandshakeMessage):
    """This class represents an EncryptedExtensions message."""

    msg_id: ClassVar = tls.MessageId.ENCRYPTED_EXTENSIONS
    """:obj:`tlsmate.tls.MessageId.ENCRYPTED_EXTENSIONS`
    """
    msg_type: ClassVar = tls.HandshakeType.ENCRYPTED_EXTENSIONS
    """:obj:`tlsmate.tls.HandshakeType.ENCRYPTED_EXTENSIONS`
    """

    extensions: List[ext.Extension] = field(default_factory=list)
    """A list of :obj:`tlsmate.ext.Extension` objects.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "EncryptedExtensions":
        return cls(extensions=ext.decode_extensions(decoder, session),)

    def get_extension(self, ext_id):
        """Get an extension from the message

        Arguments:
            ext_id: The extensions to look for

        Returns:
            The extension or None if not present.
        """
        return ext.get_extension(self.extensions, ext_id)


@dataclass
class CertificateStatus(HandshakeMessage):
    """This class represents a Certificate Status message."""

    msg_id: ClassVar = tls.MessageId.CERTIFICATE_STATUS
    """:obj:`tlsmate.tls.MessageId.CERTIFICATE_STATUS`
    """
    msg_type: ClassVar = tls.HandshakeType.CERTIFICATE_STATUS
    """:obj:`tlsmate.tls.HandshakeType.CERTIFICATE_STATUS`
    """

    status_type: tls.StatusType = tls.StatusType.OCSP
    """The type of the status, :obj:`tlsmate.tls.StatusType`
    """

    ocsp: Optional[bytes] = None
    """The status for a single status response.
    """

    ocsp_multi: Optional[List[bytes]] = None
    """The status for multiple status responses.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "CertificateStatus":
        status = decoder.enum(width=1, cls=tls.StatusType)
        if status is tls.StatusType.OCSP:
            return cls(status_type=status, ocsp=decoder.octets(width=3))

        else:
            return cls(
                status_type=status,
                ocsp_multi=[
                    decoder.octets(width=3)
                    for _ in decoder.loop_length_field(width=3, min_val=1)
                ],
            )


class ChangeCipherSpec(TlsMessage):
    """This class represents a ChangeCipherSpec message."""

    content_type: ClassVar = tls.ContentType.CHANGE_CIPHER_SPEC
    """:obj:`tlsmate.tls.ContentType.CHANGE_CIPHER_SPEC`
    """
    msg_id: ClassVar = tls.MessageId.CHANGE_CIPHER_SPEC
    """:obj:`tlsmate.tls.MessageId.CHANGE_CIPHER_SPEC`
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "ChangeCipherSpec":
        msg_type = decoder.enum(width=1, cls=tls.CCSType)
        if msg_type is not tls.CCSType.CHANGE_CIPHER_SPEC:
            raise tls.ServerMalfunction(
                tls.ServerIssue.UNKNOWN_CCS_MSG, value=int(msg_type)
            )

        return cls()

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(tls.CCSType.CHANGE_CIPHER_SPEC, width=1)

    def serialize(self, session: client_state.SessionState) -> bytes:
        """Serialize the CCS message into an octet string.

        Arguments:
            session: the :obj:`tlsmate.client_state.SessionState` object which
                holds various information required to encode some messages/extensions.

        Returns:
            the encoded message, starting with the content type
        """
        session.xcoding_msg = self.msg_id
        encoder = pdu.Encoder()
        encoder.enum(tls.CCSType.CHANGE_CIPHER_SPEC, width=1)
        return encoder.get_octet_string()


@dataclass
class Alert(TlsMessage):
    """This class represents an Alert message."""

    content_type: ClassVar = tls.ContentType.ALERT
    """:obj:`tlsmate.tls.ContentType.ALERT`
    """
    msg_id: ClassVar = tls.MessageId.ALERT
    """:obj:`tlsmate.tls.MessageId.ALERT`
    """

    level: tls.AlertLevel = tls.AlertLevel.FATAL
    """The alert level, a :obj:`tlsmate.tls.AlertLevel` object.
    """

    description: tls.AlertDescription = tls.AlertDescription.HANDSHAKE_FAILURE
    """The alert description, a :obj:`tlsmate.tls.AlertDescription` object.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "Alert":
        return cls(
            level=decoder.enum(width=1, cls=tls.AlertLevel),
            description=decoder.enum(width=1, cls=tls.AlertDescription),
        )

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(self.level, width=1)
        encoder.enum(self.description, width=1)


@dataclass
class AppData(TlsMessage):
    """This class represents an AppData message."""

    content_type: ClassVar = tls.ContentType.APPLICATION_DATA
    """:obj:`tlsmate.tls.ContentType.APPLICATION_DATA`
    """

    msg_id: ClassVar = tls.MessageId.APPLICATION_DATA
    """:obj:`tlsmate.tls.MessageId.APPLICATION_DATA`
    """

    data: bytes = b""
    """The application data.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "AppData":
        return cls(data=decoder.octets())

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.octets(self.data)


class HeartbeatMessage(TlsMessage):
    """A base class for all Heartbeat messages."""

    content_type: ClassVar = tls.ContentType.HEARTBEAT
    """:obj:`tlsmate.tls.ContentType.HEARTBEAT`
    """

    msg_type: ClassVar[tls.HeartbeatType]
    """The type of the heartbeat message.
    """

    def __init__(
        self,
        payload_length: int = 0,
        payload: Optional[bytes] = None,
        padding: Optional[bytes] = None,
    ) -> None:
        self.payload_length = payload_length
        self.payload = payload
        self.padding = padding

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "HeartbeatMessage":

        msg_type = decoder.enum(width=1, cls=tls.HeartbeatType)
        cls = _heartbeat_deserialization_map.get(msg_type)
        if cls is None:
            raise tls.ServerMalfunction(
                tls.ServerIssue.UNKNOWN_HEARTBEAT_MSG_TYPE, value=int(msg_type)
            )

        try:
            message = cls._decode(decoder, session)

        except tls.ServerMalfunction as exc:
            # enrich the exception with the message id
            exc.malfunction.message = cls.msg_id
            raise exc

        return message

    def serialize(self, session: client_state.SessionState) -> bytes:
        """Serializes a heartbeat message object into a byte stream.

        Arguments:
            session: the :obj:`tlsmate.client_state.SessionState` object which
                holds various information required to encode some messages/extensions.

        Returns:
            the encoded message, starting with the content type
        """

        encoder = pdu.Encoder()
        encoder.enum(self.msg_type, width=1)
        encoder.uint16(self.payload_length)
        encoder.octets(self.payload)
        encoder.octets(self.padding)
        return encoder.get_octet_string()


@dataclass
class HeartbeatRequest(HeartbeatMessage):
    """This class represents a heartbeat request message."""

    msg_id: ClassVar = tls.MessageId.HEARTBEAT_REQUEST
    """:obj:`tlsmate.tls.MessageId.HEARTBEAT_REQUEST`
    """
    msg_type: ClassVar = tls.HeartbeatType.HEARTBEAT_REQUEST
    """:obj:`tlsmate.tls.HeartbeatType.HEARTBEAT_REQUEST`
    """

    payload_length: int = 0
    """The payload length, can be used to set it up incorrectly (heartbleed).
    """

    payload: bytes = b""
    """The payload bytes.
    """

    padding: bytes = b""
    """The padding bytes.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "HeartbeatRequest":
        payload = decoder.octets(width=2)
        return cls(
            payload_length=len(payload), payload=payload, padding=decoder.octets()
        )


class HeartbeatResponse(HeartbeatMessage):
    """This class represents a heartbeat response message."""

    msg_id: ClassVar = tls.MessageId.HEARTBEAT_RESPONSE
    """:obj:`tlsmate.tls.MessageId.HEARTBEAT_RESPONSE`
    """
    msg_type: ClassVar = tls.HeartbeatType.HEARTBEAT_RESPONSE
    """:obj:`tlsmate.tls.HeartbeatType.HEARTBEAT_RESPONSE`
    """

    payload_length: int = 0
    """The payload length, can be used to set it up incorrectly (heartbleed).
    """

    payload: bytes = b""
    """The payload bytes.
    """

    padding: bytes = b""
    """The padding bytes.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "HeartbeatResponse":
        payload = decoder.octets(width=2)
        return cls(
            payload_length=len(payload), payload=payload, padding=decoder.octets()
        )


class SSL2Message(TlsMessage):
    """A base class for all SSL2 messages."""

    content_type: ClassVar = tls.ContentType.SSL2
    """:obj:`tlsmate.tls.ContentType.SSL2`
    """

    msg_type: ClassVar[tls.SSLMessageType]
    """The type of the SSLv2 message.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "SSL2Message":

        msg_type = decoder.enum(width=1, cls=tls.SSLMessageType)
        cls = _ssl2_deserialization_map.get(msg_type)
        if cls is None:
            raise ValueError("unknown SSL2 message type received from server")

        return cls._decode(decoder, session)


@dataclass
class SSL2ClientHello(SSL2Message):
    """This class represents an SSL2 CLientHello message."""

    msg_id: ClassVar = tls.MessageId.SSL2_CLIENT_HELLO
    """:obj:`tlsmate.tls.MessageId.SSL2_CLIENT_HELLO`
    """
    msg_type: ClassVar = tls.SSLMessageType.SSL2_CLIENT_HELLO
    """:obj:`tlsmate.tls.SSLMessageType.SSL2_CLIENT_HELLO`
    """

    version: tls.SSLVersion = tls.SSLVersion.SSL2
    """The version offered by the client.

    """
    cipher_specs: List[tls.SSLCipherKind] = field(
        default_factory=lambda: [
            tls.SSLCipherKind.SSL_CK_RC4_128_WITH_MD5,
            tls.SSLCipherKind.SSL_CK_RC4_128_EXPORT40_WITH_MD5,
            tls.SSLCipherKind.SSL_CK_RC2_128_CBC_WITH_MD5,
            tls.SSLCipherKind.SSL_CK_RC2_128_CBC_EXPORT40_WITH_MD5,
            tls.SSLCipherKind.SSL_CK_IDEA_128_CBC_WITH_MD5,
            tls.SSLCipherKind.SSL_CK_DES_64_CBC_WITH_MD5,
            tls.SSLCipherKind.SSL_CK_DES_192_EDE3_CBC_WITH_MD5,
        ]
    )
    """A list of :obj:`tlsmate.tls.SSLCipherKind` objects.
    """

    session_id: bytes = b""
    """The session id.
    """

    challenge: bytes = field(default_factory=lambda: os.urandom(16))
    """The random bytes.
    """

    def _encode(self, encoder: pdu.Encoder, session: client_state.SessionState) -> None:
        encoder.enum(self.msg_type, width=1)
        encoder.enum(self.version, width=2)
        encoder.list_length(self.cipher_specs, element_width=3, width=2)
        encoder.uint16(len(self.session_id))
        encoder.uint16(len(self.challenge))
        for ck in self.cipher_specs:
            encoder.enum(ck, width=3)

        encoder.octets(self.session_id)
        encoder.octets(self.challenge)


@dataclass
class SSL2ServerHello(SSL2Message):
    """This class represents an SSL2 ServerHello message."""

    msg_id: ClassVar = tls.MessageId.SSL2_SERVER_HELLO
    """:obj:`tlsmate.tls.MessageId.SSL2_SERVER_HELLO`
    """
    msg_type: ClassVar = tls.SSLMessageType.SSL2_SERVER_HELLO
    """:obj:`tlsmate.tls.SSLMessageType.SSL2_SERVER_HELLO`
    """

    session_id_hit: int
    """The session hit integer value.
    """

    cert_type: int
    """The type of the certificate.
    """

    version: tls.SSLVersion
    """The verion.
    """

    cipher_specs: List[tls.SSLCipherKind]
    """A list of :obj:`tlsmate.tls.SSLCipherKind` objects.
    """

    connection_id: bytes
    """The connection id.
    """

    certificate: bytes
    """The server certificate.
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "SSL2ServerHello":
        session_id_hit = decoder.uint8()
        cert_type = decoder.uint8()
        version = decoder.enum(width=2, cls=tls.SSLVersion)
        cert_len = decoder.uint16()
        cipher_spec_len = decoder.uint16()
        conn_id_len = decoder.uint16()
        certificate = decoder.octets(fixed_length=cert_len)
        cipher_specs = [
            decoder.enum(width=3, cls=tls.SSLCipherKind)
            for _ in range(int(cipher_spec_len / 3))
        ]
        connection_id = decoder.octets(fixed_length=conn_id_len)
        return cls(
            session_id_hit=session_id_hit,
            cert_type=cert_type,
            version=version,
            certificate=certificate,
            cipher_specs=cipher_specs,
            connection_id=connection_id,
        )


@dataclass
class SSL2Error(SSL2Message):
    """This class represents an SSL2 Error message."""

    msg_id: ClassVar = tls.MessageId.SSL2_ERROR
    """:obj:`tlsmate.tls.MessageId.SSL2_ERROR`
    """
    msg_type: ClassVar = tls.SSLMessageType.SSL2_ERROR
    """:obj:`tlsmate.tls.SSLMessageType.SSL2_ERROR`
    """

    error: tls.SSLError
    """The SSL error (:obj:`tlsmate.tls.SSLError`).
    """

    @classmethod
    def _decode(
        cls, decoder: pdu.Decoder, session: client_state.SessionState
    ) -> "SSL2Error":
        return cls(error=decoder.enum(width=2, cls=tls.SSLError))


"""Map the handshake message type to the corresponding class.
"""
_hs_deserialization_map: Mapping[tls.HandshakeType, Type[HandshakeMessage]] = {
    tls.HandshakeType.HELLO_REQUEST: HelloRequest,
    tls.HandshakeType.CLIENT_HELLO: ClientHello,
    tls.HandshakeType.SERVER_HELLO: ServerHello,
    tls.HandshakeType.NEW_SESSION_TICKET: NewSessionTicket,
    tls.HandshakeType.END_OF_EARLY_DATA: EndOfEarlyData,
    tls.HandshakeType.ENCRYPTED_EXTENSIONS: EncryptedExtensions,
    tls.HandshakeType.CERTIFICATE: Certificate,
    tls.HandshakeType.SERVER_KEY_EXCHANGE: ServerKeyExchange,
    tls.HandshakeType.CERTIFICATE_REQUEST: CertificateRequest,
    tls.HandshakeType.SERVER_HELLO_DONE: ServerHelloDone,
    tls.HandshakeType.CERTIFICATE_VERIFY: CertificateVerify,
    # tls.HandshakeType.CLIENT_KEY_EXCHANGE = 16
    tls.HandshakeType.FINISHED: Finished,
    tls.HandshakeType.CERTIFICATE_STATUS: CertificateStatus,
    # tls.HandshakeType.KEY_UPDATE = 24
    # tls.HandshakeType.COMPRESSED_CERTIFICATE = 25
    # tls.HandshakeType.EKT_KEY = 26
    # tls.HandshakeType.MESSAGE_HASH = 254
}


_heartbeat_deserialization_map = {
    tls.HeartbeatType.HEARTBEAT_REQUEST: HeartbeatRequest,
    tls.HeartbeatType.HEARTBEAT_RESPONSE: HeartbeatResponse,
}

_ssl2_deserialization_map: Mapping[tls.SSLMessageType, Type[SSL2Message]] = {
    tls.SSLMessageType.SSL2_SERVER_HELLO: SSL2ServerHello,
    tls.SSLMessageType.SSL2_ERROR: SSL2Error,
}
"""Map the SSL2 message type to the corresponding class.
"""


_content_type_map: Mapping[
    tls.ContentType, Callable[[pdu.Decoder, client_state.SessionState], AnyType]
] = {
    tls.ContentType.HANDSHAKE: HandshakeMessage._decode,
    tls.ContentType.ALERT: Alert._decode,
    tls.ContentType.CHANGE_CIPHER_SPEC: ChangeCipherSpec._decode,
    tls.ContentType.APPLICATION_DATA: AppData._decode,
    tls.ContentType.HEARTBEAT: HeartbeatMessage._decode,
    tls.ContentType.SSL2: SSL2Message._decode,
}


def deserialize_msg(
    message: structs.UpperLayerMsg, session: client_state.SessionState
) -> TlsMessage:
    decoder = pdu.Decoder(message.msg)
    message_obj = _content_type_map[message.content_type](decoder, session)
    if decoder.get_length():
        raise tls.ServerMalfunction(
            tls.ServerIssue.MESSAGE_LENGTH_TOO_LONG, message=message_obj.msg_id
        )

    return message_obj


def client_hello(
    profile: client_state.ClientProfile,
    session: client_state.SessionState,
    recorder: rec.Recorder,
    client_auth: client_auth.ClientAuth,
) -> ClientHello:

    """Populate a ClientHello message according to the client profile and session state.

    Returns:
        the ClientHello object
    """
    ch = ClientHello()
    max_version = max(profile.versions)
    if max_version is tls.Version.TLS13:
        ch.version = tls.Version.TLS12

    else:
        ch.version = max_version

    if profile.support_session_ticket and session.session_state_ticket is not None:
        ch.session_id = bytes.fromhex("dead beef")

    elif profile.support_session_id and session.session_state_id is not None:
        ch.session_id = session.session_state_id.session_id

    else:
        ch.session_id = b""

    ch.cipher_suites = profile.cipher_suites[:]

    ch.compression_methods = profile.compression_methods
    if ch.version == tls.Version.SSL30:
        ch.extensions = None

    else:
        if profile.support_sni:
            ch.extensions.append(ext.ServerNameIndication(host_name=session.sni))

        if profile.support_extended_master_secret:
            ch.extensions.append(ext.ExtendedMasterSecret())

        if profile.ec_point_formats is not None:
            ch.extensions.append(
                ext.EcPointFormats(ec_point_formats=profile.ec_point_formats)
            )

        if profile.supported_groups is not None:
            if max_version is tls.Version.TLS13 or bool(
                utils.filter_cipher_suites(
                    ch.cipher_suites, key_exch=[tls.KeyExchangeType.ECDH]
                )
            ):
                ch.extensions.append(
                    ext.SupportedGroups(supported_groups=profile.supported_groups)
                )

        if profile.alpn:
            ch.extensions.append(ext.ApplLayerProtNeg(protocols=profile.alpn))

        if profile.support_status_request_v2 is not tls.StatusType.NONE:
            ch.extensions.append(
                ext.StatusRequestV2(status_type=profile.support_status_request_v2)
            )

        if profile.support_status_request:
            ch.extensions.append(ext.StatusRequest())

        # RFC5246, 7.4.1.4.1.: Clients prior to TLS12 MUST NOT send this extension
        if (
            profile.signature_algorithms is not None
            and max_version >= tls.Version.TLS12
        ):
            ch.extensions.append(
                ext.SignatureAlgorithms(
                    signature_algorithms=profile.signature_algorithms
                )
            )

        if profile.support_encrypt_then_mac:
            ch.extensions.append(ext.EncryptThenMac())

        if profile.support_session_ticket:
            kwargs = {}
            if session.session_state_ticket is not None:
                kwargs["ticket"] = session.session_state_ticket.ticket

            ch.extensions.append(ext.SessionTicket(**kwargs))

        if profile.heartbeat_mode:
            ch.extensions.append(ext.Heartbeat(mode=profile.heartbeat_mode))

        if tls.Version.TLS13 in profile.versions:
            if recorder.inject(client_auth=client_auth.supported()):
                ch.extensions.append(ext.PostHandshakeAuth())

            ch.extensions.append(
                ext.SupportedVersions(versions=sorted(profile.versions, reverse=True))
            )
            # TLS13 key shares: enforce the same sequence as in supported groups
            if profile.key_shares and profile.supported_groups:
                groups = [
                    group
                    for group in profile.supported_groups
                    if group in profile.key_shares
                ]

            else:
                groups = []

            ch.extensions.append(ext.KeyShare(groups=groups))

            if profile.early_data is not None:
                ch.extensions.append(ext.EarlyData())

            if profile.support_psk and session.psks:
                ch.extensions.append(
                    ext.PskKeyExchangeMode(modes=profile.psk_key_exchange_modes)
                )
                timestamp = recorder.inject(timestamp=time.time())
                ext_psk = ext.PreSharedKey(identities=[], binders=[])

                # currently, we restrict ourselves to the oldest ticket only.
                psk = session.psks[0]
                ticket_age = int((timestamp - psk.timestamp) * 1000 + psk.age_add) % (
                    2 ** 32
                )
                ext_psk.identities.append(
                    ext.PskIdentity(
                        identity=psk.ticket, obfuscated_ticket_age=ticket_age
                    )
                )
                ext_psk.binders.append(b"\xec" * psk.hmac.mac_len)
                ch.extensions.append(ext_psk)

        for extension in profile.client_hello_extensions:
            ch.replace_extension(extension)

    return ch
