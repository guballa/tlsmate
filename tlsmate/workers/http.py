# -*- coding: utf-8 -*-
"""Module scanning for Application Layer Protocol Negotiation support
"""
# import basic stuff
import http.client
import io

# import own stuff
import tlsmate.msg as msg
import tlsmate.plugin as plg
import tlsmate.server_profile as server_profile
import tlsmate.tls as tls

# import other stuff


class _TlsmateSocket:
    def __init__(self, data=b"") -> None:
        self.data = data

    def makefile(self, *args, **kwargs):
        return io.BytesIO(self.data)


def _make_http_response(data):
    response = http.client.HTTPResponse(sock=_TlsmateSocket(data=data))
    response.begin()
    # ignore content
    response.close()
    return response


def _analyse_hsts(hsts):
    prof_hsts = server_profile.SPHsts()
    parts = [item.strip() for item in hsts.split(";")]
    prof_hsts.include_subdomain = tls.ScanState("includeSubDomains" in parts)
    prof_hsts.preload = tls.ScanState("preload" in parts)
    for item in parts:
        if item.startswith("max-age="):
            prof_hsts.max_age = int(item.split("=")[1])
            break

    return prof_hsts


def _analyse_response(prof_http, response):
    prof_http.valid_http_response = tls.ScanState.TRUE
    prof_http.status_code = response.status
    prof_http.reason = response.reason
    server = response.getheader("Server")
    if server:
        prof_http.server = server

    http_forwarding = response.getheader("Location")
    if http_forwarding:
        prof_http.http_forwarding = http_forwarding

    hsts = response.getheader("Strict-Transport-Security")
    if hsts:
        prof_http.hsts = _analyse_hsts(hsts)


class ScanHttp(plg.Worker):
    name = "http"
    descr = "scan for http headers"
    prio = 35

    def _run_http(self):
        prof_http = server_profile.SPHttp()
        values = self.server_profile.get_profile_values(
            [
                tls.Version.SSL30,
                tls.Version.TLS10,
                tls.Version.TLS11,
                tls.Version.TLS12,
                tls.Version.TLS13,
            ],
            full_hs=True,
        )
        if not values.versions:
            prof_http.valid_http_response = tls.ScanState.NA
            return prof_http

        self.client.init_profile(profile_values=values)

        app_resp = None
        with self.client.create_connection() as conn:
            conn.handshake()
            req = f"GET / HTTP/1.1\r\nHost: {self.client.session.sni}\r\n\r\n"
            conn.send(msg.AppData(req.encode()))
            app_resp = conn.wait(msg.AppData)

        if app_resp:
            try:
                response = _make_http_response(app_resp.data)

            except http.client.HTTPException:
                prof_http.valid_http_response = tls.ScanState.FALSE
                return prof_http

            _analyse_response(prof_http, response)

        else:
            prof_http.valid_http_response = tls.ScanState.UNDETERMINED

        return prof_http

    def run(self) -> None:
        self.server_profile.allocate_http()
        self.server_profile.http = self._run_http()
