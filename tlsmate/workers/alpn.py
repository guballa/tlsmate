# -*- coding: utf-8 -*-
"""Module scanning for Application Layer Protocol Negotiation support
"""
# import basic stuff
from typing import Optional, List, Tuple

# import own stuff
import tlsmate.msg as msg
import tlsmate.plugin as plg
import tlsmate.server_profile as server_profile
import tlsmate.tls as tls

# import other stuff


class ScanAlpn(plg.Worker):
    name = "alpn"
    descr = "scan for ALPN support"
    prio = 30

    def _get_proto_from_server(self) -> Optional[tls.Alpn]:
        proto = None
        with self.client.create_connection() as conn:
            conn.send(msg.ClientHello)
            alpn_msg = conn.wait(msg.ServerHello)
            if conn.version is tls.Version.TLS13:
                conn.wait(msg.ChangeCipherSpec, optional=True)
                alpn_msg = conn.wait(msg.EncryptedExtensions)

            if alpn_msg:
                alpn = alpn_msg.get_extension(
                    tls.Extension.APPLICATION_LAYER_PROTOCOL_NEGOTIATION
                )
                if alpn:
                    if len(alpn.protocol_name_list) != 1:
                        raise tls.ServerMalfunction(
                            tls.ServerIssue.UNEXPECTED_NBR_OF_ELEMENTS,
                            message=alpn_msg.msg_type,
                            extension=tls.Extension.APPLICATION_LAYER_PROTOCOL_NEGOTIATION,  # noqa
                        )

                    if alpn.protocol_name_list[0] not in self.client.profile.alpn:
                        raise tls.ServerMalfunction(
                            tls.ServerIssue.ILLEGAL_PARAMETER_VALUE,
                            message=alpn_msg.msg_type,
                            extension=tls.Extension.APPLICATION_LAYER_PROTOCOL_NEGOTIATION,  # noqa
                        )

                    proto = alpn.protocol_name_list[0]

        return proto

    def _get_alpn_list(self) -> List[tls.Alpn]:
        self.client.profile.alpn = tls.Alpn.all()
        supported = []
        while self.client.profile.alpn:
            proto = self._get_proto_from_server()
            if not proto:
                break

            supported.append(proto)
            self.client.profile.alpn.remove(proto)

        return supported

    def _determine_server_preference(
        self, supported: List[tls.Alpn]
    ) -> Tuple[tls.ScanState, List[tls.Alpn]]:
        if len(supported) < 2:
            return tls.ScanState.NA, supported

        offered = supported[:]
        self.client.profile.alpn = offered
        proto = self._get_proto_from_server()
        if not proto:
            return tls.ScanState.UNDETERMINED, supported

        if proto == offered[0]:
            offered.append(offered.pop(0))
            proto = self._get_proto_from_server()
            if not proto:
                return tls.ScanState.UNDETERMINED, supported

            if proto != offered[-1]:
                return tls.ScanState.FALSE, supported

        ordered = [proto]
        offered.remove(proto)
        while len(offered) > 1:
            proto = self._get_proto_from_server()
            if not proto:
                return tls.ScanState.UNDETERMINED, supported

            ordered.append(proto)
            offered.remove(proto)

        ordered.append(offered[0])
        return tls.ScanState.TRUE, ordered

    def run(self) -> None:
        values = self.server_profile.get_profile_values(
            [tls.Version.TLS10, tls.Version.TLS11, tls.Version.TLS12, tls.Version.TLS13]
        )
        self.client.init_profile(profile_values=values)
        profile_alpn = server_profile.SPAlpn()
        supported = self._get_alpn_list()
        if supported:
            preference, supported = self._determine_server_preference(supported)
            profile_alpn.server_preference = preference  # type: ignore
            profile_alpn.protocols = [  # type: ignore
                prot.value.decode() for prot in supported
            ]

        profile_alpn.extension_supported = tls.ScanState(  # type: ignore
            bool(supported)
        )
        self.server_profile.allocate_features()
        self.server_profile.features.alpn = profile_alpn
