# -*- coding: utf-8 -*-
"""Module scanning for TLS protocol versions, cipher suites and certificate chains
"""
# import basic stuff
import enum
import logging

# import own stuff
import tlsmate.msg as msg
import tlsmate.plugin as plg
import tlsmate.server_profile as server_profile
import tlsmate.tls as tls
import tlsmate.utils as utils

# import other stuff


class ScanCipherSuites(plg.Worker):
    """Scans for the supported versions, cipher suites and certificate chains.

    The results are stored in the server profile.
    """

    class Status(enum.Enum):
        OK = enum.auto()
        SNI_REJECTED = enum.auto()
        VERSION_NOT_SUPPORTED = enum.auto()

    name = "cipher_suites"
    descr = "scan for supported TLS versions, cipher suites and certificates"
    prio = 10

    config_mapping = {
        tls.Version.SSL20: "sslv2",
        tls.Version.SSL30: "sslv3",
        tls.Version.TLS10: "tls10",
        tls.Version.TLS11: "tls11",
        tls.Version.TLS12: "tls12",
        tls.Version.TLS13: "tls13",
    }

    supported_groups_tls12 = [
        tls.SupportedGroups.X25519,
        tls.SupportedGroups.X448,
        tls.SupportedGroups.SECT163K1,
        tls.SupportedGroups.SECT163R2,
        tls.SupportedGroups.SECT233K1,
        tls.SupportedGroups.SECT233R1,
        tls.SupportedGroups.SECT283K1,
        tls.SupportedGroups.SECT283R1,
        tls.SupportedGroups.SECT409K1,
        tls.SupportedGroups.SECT409R1,
        tls.SupportedGroups.SECT571K1,
        tls.SupportedGroups.SECT571R1,
        tls.SupportedGroups.SECP224R1,
        tls.SupportedGroups.SECP256K1,
        tls.SupportedGroups.BRAINPOOLP256R1,
        tls.SupportedGroups.BRAINPOOLP384R1,
        tls.SupportedGroups.BRAINPOOLP512R1,
        tls.SupportedGroups.SECP256R1,
        tls.SupportedGroups.SECP384R1,
        tls.SupportedGroups.SECP521R1,
        tls.SupportedGroups.FFDHE2048,
        tls.SupportedGroups.FFDHE4096,
    ]

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self._cert_version = None
        self._cert_cs = None
        self._sni_rejected = False

    def _get_server_cs_and_cert(self, version):
        """Performs a handshake and retieves the cipher suite and certificate chain.

        The certificate chain received from the server is added to the server profile.

        Arguments:
            version (:obj:`tlsmate.tls.Version`): The TLS version

        Returns:
            :obj:`tlsmate.tls.CipherSuite`: the cipher suite selected by the server.
                None, if no SeverHello was received.
        """

        with self.client.create_connection() as conn:
            conn.send(msg.ClientHello)
            message = conn.wait(msg.Any)

            if isinstance(message, msg.ServerHello):
                if version is not message.get_version():
                    return self.Status.OK, None

                if version is tls.Version.TLS13:
                    conn.wait(msg.ChangeCipherSpec, optional=True)
                    conn.wait(msg.EncryptedExtensions)

                certificate = conn.wait(msg.Certificate, optional=True)
                if certificate is not None:
                    self.server_profile.append_unique_cert_chain(conn.cert_chain)
                    if not self._cert_version and version is not tls.Version.SSL30:
                        # remember the version and cs that reveals a certificate.
                        self._cert_version = version
                        self._cert_cs = message.cipher_suite

                return self.Status.OK, message.cipher_suite

            elif isinstance(message, msg.Alert):
                if message.description is tls.AlertDescription.UNRECOGNIZED_NAME:
                    return self.Status.SNI_REJECTED, None

                elif message.description is tls.AlertDescription.PROTOCOL_VERSION:
                    return self.Status.VERSION_NOT_SUPPORTED, None

        return self.Status.OK, None

    def _get_server_cs(self):
        """Performs a handshake and returns the ciper suite selected by the server.

        Returns:
            :obj:`tlsmate.tls.CipherSuite`: the cipher suite selected by the server.
                None, if no SeverHello was received.
        """

        server_hello = None
        with self.client.create_connection() as conn:
            conn.send(msg.ClientHello)
            server_hello = conn.wait(msg.Any)

        return None if server_hello is None else server_hello.cipher_suite

    def _get_server_preference(self, cipher_suites):
        """Determine the server preference order of supported cipher suites.

        Arguments:
            cipher_suites (list of :obj:`tlsmate.tls.CipherSuite`): the list of
                cipher suites support by the server in arbitrary order.

        Returns:
            list of :obj:`tlsmate.tls.CipherSuite`: the cipher suites supported by
                the server in the order of preference.
        """
        self.client.profile.cipher_suites = cipher_suites
        server_pref = []
        while self.client.profile.cipher_suites:
            server_cs = self._get_server_cs()
            if server_cs is None or server_cs not in self.client.profile.cipher_suites:
                return None

            server_pref.append(server_cs)
            self.client.profile.cipher_suites.remove(server_cs)

        return server_pref

    def _chacha_poly_pref(self, server_pref, ciphers):
        if server_pref is not tls.ScanState.TRUE:
            return tls.ScanState.NA

        tmp_cs = ciphers.cipher_suites[:]
        chacha_cs = utils.filter_cipher_suites(
            tmp_cs, cipher_prim=[tls.CipherPrimitive.CHACHA], remove=True
        )
        if not chacha_cs:
            return tls.ScanState.NA

        check_chacha_pref = False
        for idx, cs in enumerate(ciphers.cipher_suites):
            if cs not in chacha_cs:
                if idx < len(chacha_cs):
                    check_chacha_pref = True

                break

        if not check_chacha_pref:
            return tls.ScanState.NA

        chacha_cs.extend(tmp_cs)
        self.client.profile.cipher_suites = chacha_cs
        with self.client.create_connection() as conn:
            conn.send(msg.ClientHello)
            server_hello = conn.wait(msg.ServerHello)
            if server_hello.cipher_suite in chacha_cs:
                return tls.ScanState.TRUE

            else:
                return tls.ScanState.FALSE

        return tls.ScanState.UNDETERMINED

    def _tls_enum_version(self, version, vers_prof):
        """Determines the supported cipher suites and other stuff for a given version.

        Determines the supported cipher suites, if the server enforces the priority
        (and if so, determines the order of cipher suites according to their priority)
        and extracts the certificate chains and stores them in the server profile.

        Arguments:
            version (:obj:`tlsmate.tls.Version`): the TLS version to enumerate.
            vers_prof (:obj:`tlsmate.server_profile.SPVersion`): the version profile
        """

        if version is tls.Version.TLS13:
            self.client.profile.supported_groups = tls.SupportedGroups.all_tls13()

        else:
            self.client.profile.supported_groups = self.supported_groups_tls12

        cipher_suites = utils.filter_cipher_suites(
            tls.CipherSuite.all(), version=version
        )

        if version is tls.Version.TLS12:
            # put all CHACHA_POLY cipher suites at the end, so that there is no
            # interference with potential "chacha_poly_preference".
            chacha_cs = utils.filter_cipher_suites(
                cipher_suites, cipher_prim=[tls.CipherPrimitive.CHACHA], remove=True
            )
            cipher_suites.extend(chacha_cs)

        self.client.profile.versions = [version]
        supported_cs = []

        # get a list of all supported cipher suites, don't send more than
        # max_items cipher suites in the ClientHello
        max_items = 32
        while cipher_suites:
            sub_set = cipher_suites[:max_items]
            cipher_suites = cipher_suites[max_items:]

            while sub_set:
                self.client.profile.cipher_suites = sub_set
                status, cipher_suite = self._get_server_cs_and_cert(version)

                if status is self.Status.SNI_REJECTED:
                    self._sni_rejected = True
                    if not self.client.profile.support_sni or supported_cs:
                        self.client.session.report_server_issue(
                            tls.ServerIssue.SNI_REJECTED
                        )
                        vers_prof.support = tls.ScanState.UNDETERMINED
                        return

                    self.client.profile.support_sni = False
                    continue

                elif status is self.Status.VERSION_NOT_SUPPORTED:
                    if supported_cs:
                        self.client.session.report_server_issue(
                            tls.ServerIssue.VERSION_NOT_SUPPORTED
                        )

                    vers_prof.support = tls.ScanState.FALSE
                    return

                if cipher_suite not in (None, tls.CipherSuite.TLS_NULL_WITH_NULL_NULL):
                    if cipher_suite not in sub_set:
                        # Server responds with a cipher suite that was not offered.
                        # Treat this as no offered cipher suite is supported.
                        sub_set = []

                    else:
                        sub_set.remove(cipher_suite)
                        supported_cs.append(cipher_suite)

                else:
                    sub_set = []

        if supported_cs:
            if len(supported_cs) == 1:
                server_prio = tls.ScanState.NA

            else:
                server_prio = tls.ScanState.FALSE
                # check if server enforce the cipher suite prio
                self.client.profile.cipher_suites = supported_cs
                if self._get_server_cs() != supported_cs[0]:
                    server_prio = tls.ScanState.TRUE

                else:
                    supported_cs.append(supported_cs.pop(0))
                    if self._get_server_cs() != supported_cs[0]:
                        server_prio = tls.ScanState.TRUE

                # determine the order of cipher suites on server side, if applicable
                if server_prio == tls.ScanState.TRUE:
                    res = self._get_server_preference(supported_cs)
                    if res is None:
                        server_prio = tls.ScanState.UNDETERMINED

                    else:
                        supported_cs = res

                else:
                    # esthetical: restore original order, which means the cipher suites
                    # are ordered according to the binary representation
                    supported_cs.insert(0, supported_cs.pop())

            ciphers = server_profile.SPCiphers(
                server_preference=server_prio, cipher_suites=supported_cs
            )
            if version is tls.Version.TLS12:
                ciphers.chacha_poly_preference = self._chacha_poly_pref(
                    server_prio, ciphers
                )

            vers_prof.ciphers = ciphers
            vers_prof.support = tls.ScanState.TRUE

        else:
            vers_prof.support = tls.ScanState.FALSE

    def _ssl2_enum_version(self, vers_prof):
        """Minimal support to determine if SSLv2 is supported.
        """
        with self.client.create_connection() as conn:
            conn.send(msg.SSL2ClientHello)
            server_hello = conn.wait(msg.SSL2ServerHello)
            if server_hello is not None:
                vers_prof.cipher_kinds = server_hello.cipher_specs
                vers_prof.server_preference = tls.ScanState.UNDETERMINED
                vers_prof.version = tls.Version.SSL20
                vers_prof.support = tls.ScanState.TRUE
                return

        vers_prof.support = tls.ScanState.FALSE

    def _enum_version(self, version, vers_prof):
        """Scan a specific TLS version.

        Arguments:
            version (:obj:`tlsmate.tls.Version`): The TLS version to enumerate.
        """
        if version is tls.Version.SSL20:
            self._ssl2_enum_version(vers_prof)

        else:
            self._tls_enum_version(version, vers_prof)

    def _determine_sni_status(self):
        self.server_profile.allocate_server()
        if self._sni_rejected:
            status = tls.SniStatus.REJECTED

        else:
            if self._cert_version:
                self.client.profile.versions = [self._cert_version]
                self.client.profile.cipher_suites = [self._cert_cs]
                self.client.profile.support_sni = False
                with self.client.create_connection() as conn:
                    conn.handshake()

                if conn.msg.server_certificate:
                    if self.server_profile.cert_chain_present(
                        conn.msg.server_certificate.get_cert_chain()
                    ):
                        status = tls.SniStatus.OPTIONAL

                    else:
                        status = tls.SniStatus.REQUIRED

                else:
                    status = tls.SniStatus.REQUIRED

            else:
                status = tls.SniStatus.NA

        self.server_profile.server.sni_status = status

    def run(self):
        """The entry point for the worker.
        """

        self.client.alert_on_invalid_cert = False
        self.client.profile.support_extended_master_secret = False
        self.client.profile.support_encrypt_then_mac = False
        self.client.profile.support_session_id = False
        self.client.profile.support_session_ticket = False
        self.client.profile.ec_point_formats = None
        self.client.profile.key_shares = tls.SupportedGroups.all_tls13()
        self.client.profile.signature_algorithms = [
            tls.SignatureScheme.ED25519,
            tls.SignatureScheme.ED448,
            tls.SignatureScheme.ECDSA_SECP384R1_SHA384,
            tls.SignatureScheme.ECDSA_SECP256R1_SHA256,
            tls.SignatureScheme.ECDSA_SECP521R1_SHA512,
            tls.SignatureScheme.RSA_PSS_RSAE_SHA256,
            tls.SignatureScheme.RSA_PSS_RSAE_SHA384,
            tls.SignatureScheme.RSA_PSS_RSAE_SHA512,
            tls.SignatureScheme.RSA_PKCS1_SHA256,
            tls.SignatureScheme.RSA_PKCS1_SHA384,
            tls.SignatureScheme.RSA_PKCS1_SHA512,
            tls.SignatureScheme.ECDSA_SHA1,
            tls.SignatureScheme.RSA_PKCS1_SHA1,
        ]

        self.server_profile.allocate_versions()
        for version in tls.Version.all():
            vers_prof = server_profile.SPVersion(version=version)
            if self.config.get(self.config_mapping[version]):
                logging.info(f"starting to enumerate {version}")
                self._enum_version(version, vers_prof)
                logging.info(f"enumeration for {version} finished")

            else:
                vers_prof.support = tls.ScanState.UNDETERMINED

            self.server_profile.versions.append(vers_prof)

        self._determine_sni_status()
