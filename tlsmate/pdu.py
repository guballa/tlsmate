# -*- coding: utf-8 -*-
"""Module implementing helper functions for handling protocol data units
"""
# import basic stuff
import struct
from typing import List, Any, Generator, Mapping, Callable, Type

# import own stuff
import tlsmate.tls as tls

# import other stuff


class Encoder(object):
    """Class for encoding python items to TLS messages"""

    def reset(self) -> None:
        """Resets the encoder to its initial state
        """

        self.parts: List[bytes] = []
        self.length = 0
        self.marker = 0

    def __init__(self) -> None:
        self.reset()

    def set_marker(self) -> None:
        """Sets a marker at the current read pointer
        """

        self.marker = self.length

    def distance_to_marker(self) -> int:
        """Returns the number of bytes from the marker to the end of the buffer
        """

        return self.length - self.marker

    def append_encoder(self, encoder: "Encoder"):
        """Extend the current encoder with the content of the given encoder."""

        self.parts.extend(encoder.parts)
        self.length += encoder.length

    def get_octet_string(self) -> bytes:
        """Returns the encoded octet string"""

        return b"".join(self.parts)

    def get_length(self) -> int:
        """Returns the length of the current encoded message"""

        return self.length

    def uint8(self, val: int) -> None:
        """Encodes an integer as an uint8"""

        self.parts.append(struct.pack("!B", val))
        self.length += 1

    def uint16(self, val: int) -> None:
        """Encodes an integer as an uint16"""

        self.parts.append(struct.pack("!H", val))
        self.length += 2

    def uint24(self, val: int) -> None:
        """Encodes an integer as an uint24"""

        self.parts.append(struct.pack("!I", val)[1:])
        self.length += 3

    def uint32(self, val: int) -> None:
        """Encodes an integer as an uint32"""

        self.parts.append(struct.pack("!I", val))
        self.length += 4

    _map_width = {1: uint8, 2: uint16, 3: uint24, 4: uint32}

    def length_field(self, val: int, width: int) -> None:
        """Encodes a length indicator"""

        self._map_width[width](self, val)

    def octets(self, val: bytes, width: int = 0) -> None:
        """Encodes an octet string"""

        length = len(val)
        if width:
            self.length_field(length, width)

        self.parts.append(val)
        self.length += length

    def string(self, val: str, width=0) -> None:
        """Encodes an ASCII string"""

        self.octets(val.encode(), width)

    def enum(self, val: Any, width: int) -> None:
        """Encodes an integer enum"""

        self.length_field(int(val), width)

    def list_length(self, array: List, element_width: int, width: int) -> None:
        """Encodes a length field for a uniform list"""

        self.length_field(len(array) * element_width, width)

    def prepend_last_item(self) -> None:
        """Prepends the encoded message with the last added item"""

        self.parts.insert(0, self.parts.pop())

    def add_own_length(self, width: int) -> None:
        """Prepends the encoded message by the current length"""

        self.length_field(self.length, width)
        self.parts.insert(0, self.parts.pop())


class Decoder(object):
    """Class to decode a TLS message into python objects"""

    def __init__(self, octets: bytes = b"") -> None:
        self.octet_str = octets
        self.length = len(octets)
        self.read_ptr = 0
        self.marker = 0

    def flush(self) -> None:
        """Drop all octets from the start of the buffer to the current position
        """

        self.octet_str = self.octet_str[self.read_ptr :]
        self.length = self.length - self.read_ptr
        self.read_ptr = 0
        self.marker = 0

    @staticmethod
    def check_range(
        val: int, issue: tls.ServerIssue, min_val: int = 0, max_val: int = 0,
    ) -> int:
        if val < min_val:
            raise tls.ServerMalfunction(issue)

        if max_val and val > max_val:
            raise tls.ServerMalfunction(issue)

        return val

    def append(self, octets: bytes) -> None:
        """Appends the given octet string to the decoder.
        """

        self.octet_str += octets
        self.length += len(octets)

    def get_length(self) -> int:
        """Returns the number of remaining octets"""

        return self.length - self.read_ptr

    def uint8(
        self, min_val=0, max_val=0, issue=tls.ServerIssue.PARAMETER_OUT_OF_RANGE
    ) -> int:
        """Deserializes a uint8"""

        if self.read_ptr + 1 > self.length:
            raise tls.ServerMalfunction(tls.ServerIssue.LENGTH_TOO_SHORT)

        (val,) = struct.unpack("!B", self.octet_str[self.read_ptr : self.read_ptr + 1])
        self.read_ptr += 1
        return self.check_range(val, min_val=min_val, max_val=max_val, issue=issue)

    def uint16(
        self, min_val=0, max_val=0, issue=tls.ServerIssue.PARAMETER_OUT_OF_RANGE
    ) -> int:
        """Deserializes a uint16"""

        if self.read_ptr + 2 > self.length:
            raise tls.ServerMalfunction(tls.ServerIssue.LENGTH_TOO_SHORT)

        (val,) = struct.unpack("!H", self.octet_str[self.read_ptr : self.read_ptr + 2])
        self.read_ptr += 2
        return self.check_range(val, min_val=min_val, max_val=max_val, issue=issue)

    def uint24(
        self, min_val=0, max_val=0, issue=tls.ServerIssue.PARAMETER_OUT_OF_RANGE
    ) -> int:
        """Deserializes a uint24"""

        if self.read_ptr + 3 > self.length:
            raise tls.ServerMalfunction(tls.ServerIssue.LENGTH_TOO_SHORT)

        high_byte, val = struct.unpack(
            "!BH", self.octet_str[self.read_ptr : self.read_ptr + 3]
        )
        self.read_ptr += 3
        return self.check_range(
            0x10000 * high_byte + val, min_val=min_val, max_val=max_val, issue=issue
        )

    def uint32(
        self, min_val=0, max_val=0, issue=tls.ServerIssue.PARAMETER_OUT_OF_RANGE
    ) -> int:
        """Deserializes a uint32"""

        if self.read_ptr + 4 > self.length:
            raise tls.ServerMalfunction(tls.ServerIssue.LENGTH_TOO_SHORT)

        (val,) = struct.unpack("!I", self.octet_str[self.read_ptr : self.read_ptr + 4])
        self.read_ptr += 4
        return self.check_range(val, min_val=min_val, max_val=max_val, issue=issue)

    _map_width: Mapping[int, Callable] = {1: uint8, 2: uint16, 3: uint24, 4: uint32}

    def length_field(self, width, min_val=0, max_val=0) -> int:
        """Deserializes a length field"""

        val = self._map_width[width](
            self,
            min_val=min_val,
            max_val=max_val,
            issue=tls.ServerIssue.LENGTH_OUT_OF_RANGE,
        )
        if self.read_ptr + val > self.length:
            raise tls.ServerMalfunction(tls.ServerIssue.LENGTH_TOO_SHORT)

        return val

    def enum(self, width: int, cls: Type[tls.ExtendedEnum], strict=False) -> Any:
        """Deserializes an enum"""

        val = self._map_width[width](self)
        return cls.val2enum(val, alert_on_failure=strict) or val

    def octets(self, fixed_length=None, width=0, min_val=0, max_val=0) -> bytes:
        """Decode octets"""
        if width:
            length = self.length_field(width, min_val, max_val)

        elif fixed_length is not None:
            length = fixed_length

        else:
            # if no constraints on the length: return all bytes remaining
            length = self.length - self.read_ptr

        if length > self.length - self.read_ptr:
            raise tls.ServerMalfunction(tls.ServerIssue.LENGTH_TOO_SHORT)

        octets = self.octet_str[self.read_ptr : self.read_ptr + length]
        self.read_ptr += length
        return octets

    def string(self, fixed_length=0, width=0, min_val=0, max_val=0) -> str:
        """Decode ASCII string
        """

        return self.octets(
            fixed_length=fixed_length, width=width, min_val=min_val, max_val=max_val
        ).decode()

    def loop_length_field(self, width, min_val: int = 0, max_val: int = 0) -> Generator:
        """Executes a for loop until "length" number of octets are deserialized"""

        orig_length = self.length
        length = self.length_field(width, min_val=min_val, max_val=max_val)
        self.length = self.read_ptr + length
        while self.read_ptr < self.length:
            yield None

        self.length = orig_length

    def skip(self, amount: int) -> None:
        """Skip the number of octets given.
        """

        if self.read_ptr + amount > self.length:
            raise tls.ServerMalfunction(tls.ServerIssue.LENGTH_TOO_SHORT)

        self.read_ptr += amount

    def set_marker(self) -> None:
        """Sets a marker at the current read pointer
        """

        self.marker = self.read_ptr

    def from_marker_to_here(self) -> bytes:
        """Returns the bytes from the marker to the current read pointer
        """

        return self.octet_str[self.marker : self.read_ptr]


def pack_uint8(val: int) -> bytes:
    """Packs a uint8 value into a byte object.

    Arguments:
        val: The value to pack.

    Returns:
        The bytes object representing the value in network order (big endian).

    Raises:
        ValueError: If the value cannot be represented in one byte.
    """

    if val > 0xFF:
        raise ValueError(f"Cannot pack {val} into 1 byte")

    return struct.pack("!B", val)


def pack_uint16(val: int) -> bytes:
    """Packs a uint16 value into a byte object.

    Arguments:
        val: The value to pack.

    Returns:
        The bytes object representing the value in network order (big endian).

    Raises:
        ValueError: If the value cannot be represented in two bytes.
    """

    if val > 0xFFFF:
        raise ValueError(f"Cannot pack {val} into 2 bytes")

    return struct.pack("!H", val)


def pack_uint32(val: int) -> bytes:
    """Packs a uint32 value into a byte object.

    Arguments:
        val: The value to pack.

    Returns:
        The bytes object representing the value in network order (big endian).

    Raises:
        ValueError: If the value cannot be represented in four bytes.
    """

    if val > 0xFFFFFFFF:
        raise ValueError(f"Cannot pack {val} into 4 bytes")

    return struct.pack("!I", val)


def pack_uint64(val: int) -> bytes:
    """Packs a uint64 value into a byte object.

    Arguments:
        val: The value to pack.

    Returns:
        The bytes object representing the value in network order (big endian).

    Raises:
        ValueError: If the value cannot be represented in eight bytes.
    """

    if val > 0xFFFFFFFFFFFFFFFF:
        raise ValueError(f"Cannot pack {val} into 8 bytes")

    return struct.pack("!Q", val)


def dump(data: bytes, separator: str = " ", with_length: bool = True) -> str:
    """Provide a human readable representation of a bytes object.

    Arguments:
        data: The data to represent
        separator: the separator character(s) between the bytes. Defaults to " ".
        with_length: indication, if the length shall be appended in brackets.

    Returns:
        A human readable string, with a blank between each byte, and the length
        of the string appended in brackets.
    """

    ret = separator.join(f"{y:02x}" for y in data)
    if with_length:
        ret = ret + f" ({len(data)})"

    return ret


def dump_short(
    data: bytes,
    separator: str = " ",
    with_length: bool = True,
    start: int = 10,
    end: int = 10,
) -> str:
    """Like dump, but shorten the hexdump if it exceeds a certain limit.

        If the data is longer than start + end, then only a part of the data will
        be returned, and the middle of the data is collapsed to " ... ".

    Arguments:
        data: The data to represent
        separator: the separator character(s) between the bytes. Defaults to " ".
        with_length: indication, if the length shall be appended in brackets.
        start: the first number of bytes to display
        end: the last number of bytes to display.

    Returns:
        A human readable string, with a blank between each byte, and the length
        of the string appended in brackets.
    """

    length = len(data)
    if length <= start + end:
        return dump(data, separator, with_length)

    else:
        ret = (
            f"{dump(data[:start], separator=separator, with_length=False)} ... "
            f"{dump(data[-end:], separator=separator, with_length=False)}"
        )
        if with_length:
            ret += f" ({length})"

        return ret


def string(data: bytes) -> str:
    """Simple version of dump, separating the data with a colon, and omit the length

    Arguments:
        data (bytes): The data to represent

    Returns:
        str: A human readable string, with a colon between each byte.
    """

    return dump(data, separator=":", with_length=False)
