# -*- coding: utf-8 -*-
"""Implements a class to be used for unit testing.
"""
import pathlib
import tlsmate.msg as msg
import tlsmate.tls as tls
import tlsmate.tlssuite as tlssuite


class TestCase(tlssuite.TlsSuiteTester):
    """Class used for tests with pytest.

    For more information refer to the documentation of the TcRecorder class.
    """

    cipher_suite = tls.CipherSuite.TLS_RSA_EXPORT_WITH_RC4_40_MD5

    recorder_yaml = cipher_suite.name
    path = pathlib.Path(__file__)
    server_cmd = (
        "utils/start_openssl --version {library} --port {server_port} "
        "--cert1 server-rsa --no-cert-chain  --ca-file ca-certificates "
        "-- -www -cipher ALL"
    )
    library = tlssuite.TlsLibrary.openssl1_0_1e

    server = "localhost"

    def run(self, tlsmate, is_replaying):
        client = tlsmate.client
        client.set_profile(tls.Profile.LEGACY)
        client.profile.versions = [tls.Version.TLS10]
        client.cipher_suites = [self.cipher_suite]
        end_of_tc_reached = False
        with client.create_connection() as conn:
            conn.send(msg.ClientHello)
            conn.wait(msg.ServerHello)
            conn.wait(msg.Certificate)
            conn.wait(msg.ServerKeyExchange)
            conn.wait(msg.ServerHelloDone)
            end_of_tc_reached = True

        assert end_of_tc_reached is True


if __name__ == "__main__":
    TestCase().entry(is_replaying=False)
