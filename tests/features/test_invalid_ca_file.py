# -*- coding: utf-8 -*-
"""Implements a class to be used for unit testing.
"""
import pathlib
import tlsmate.msg as msg
import tlsmate.tls as tls
import tlsmate.tlssuite as tlssuite


class TestCase(tlssuite.TlsSuiteTester):
    """Class used for tests with pytest.

    For more information refer to the documentation of the TcRecorder class.
    """

    cipher_suite = tls.CipherSuite.TLS_RSA_EXPORT_WITH_RC4_40_MD5

    recorder_yaml = "invalid_ca_file"
    path = pathlib.Path(__file__)
    server_cmd = (
        "utils/start_openssl --version {library} --port {server_port} "
        "--cert1 server-rsa --no-cert-chain  --ca-file ca-certificates "
        "-- -www -cipher ALL"
    )
    library = tlssuite.TlsLibrary.openssl1_0_1e

    server = "localhost"

    def run(self, tlsmate, is_replaying):
        client = tlsmate.client
        client.set_profile(tls.Profile.LEGACY)
        client.profile.versions = [tls.Version.TLS10]
        client.cipher_suites = [self.cipher_suite]
        tlsmate.trust_store._ca_files = None
        tlsmate.trust_store.set_ca_files(["/tmp"])
        try:
            with client.create_connection() as conn:
                conn.send(msg.ClientHello)
                conn.wait(msg.ServerHello)
                conn.wait(msg.Certificate)
                assert False

            assert False

        except tls.UserInputError:
            assert True


if __name__ == "__main__":
    TestCase().entry(is_replaying=False)
