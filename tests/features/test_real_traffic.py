# -*- coding: utf-8 -*-
"""Test connection to external server.
"""
import tlsmate.tls as tls


def test_main(tlsmate, isrg_root_x1_ca_file):
    tlsmate.trust_store.set_ca_files([isrg_root_x1_ca_file])
    tlsmate.client.set_profile(tls.Profile.INTEROPERABILITY)
    with tlsmate.client.create_connection("mozilla-modern.badssl.com") as conn:
        conn.handshake()

    assert conn.handshake_completed
