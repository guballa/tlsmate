# -*- coding: utf-8 -*-
"""Test setting up extensions for ClientHello via profile.
"""
import tlsmate.ext as ext
import tlsmate.tls as tls


def test_main(tlsmate):
    client = tlsmate.client
    client.set_profile(tls.Profile.INTEROPERABILITY)
    client.profile.client_hello_extensions = []
    ch = client.client_hello()
    nbr_ext = len(ch.extensions)

    client.profile.client_hello_extensions = [
        ext.StatusRequest(),
        ext.StatusRequestV2(status_type=tls.StatusType.OCSP_MULTI),
    ]
    ch = client.client_hello()
    assert len(ch.extensions) == nbr_ext + 2
    assert ch.extensions[-2].extension_id is tls.Extension.STATUS_REQUEST
    assert ch.extensions[-1].extension_id is tls.Extension.STATUS_REQUEST_V2
