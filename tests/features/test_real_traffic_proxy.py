# -*- coding: utf-8 -*-
"""Test connection to external server via a proxy.
"""
import tlsmate.tls as tls


def test_main(tlsmate, isrg_root_x1_ca_file, proxy):
    tlsmate.trust_store.set_ca_files([isrg_root_x1_ca_file])
    tlsmate.config.set("proxy", proxy)
    tlsmate.client.set_profile(tls.Profile.INTEROPERABILITY)
    with tlsmate.client.create_connection("mozilla-modern.badssl.com") as conn:
        conn.handshake()

    assert conn.handshake_completed
