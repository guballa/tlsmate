# -*- coding: utf-8 -*-
"""Implement unit tests for the module pdu.
"""
from tlsmate import pdu
import pytest


def test_pack_uint8_overflow():
    val = 0xFF
    assert pdu.pack_uint8(val) == bytes.fromhex("ff")
    with pytest.raises(ValueError, match=f"Cannot pack {val + 1} into 1 byte"):
        pdu.pack_uint8(val + 1)


def test_pack_uint16_overflow():
    val = 0xFFFF
    assert pdu.pack_uint16(val) == bytes.fromhex("ff ff")
    with pytest.raises(ValueError, match=f"Cannot pack {val +1 } into 2 bytes"):
        pdu.pack_uint16(val + 1)


def test_pack_uint32_overflow():
    val = 0xFFFFFFFF
    assert pdu.pack_uint32(val) == bytes.fromhex("ff ff ff ff")
    with pytest.raises(ValueError, match=f"Cannot pack {val +1 } into 4 bytes"):
        pdu.pack_uint32(val + 1)


def test_pack_uint64_overflow():
    val = 0xFFFFFFFFFFFFFFFF
    assert pdu.pack_uint64(val) == bytes.fromhex("ff ff ff ff ff ff ff ff")
    with pytest.raises(ValueError, match=f"Cannot pack {val +1 } into 8 bytes"):
        pdu.pack_uint64(val + 1)
