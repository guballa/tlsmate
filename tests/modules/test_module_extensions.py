# -*- coding: utf-8 -*-

# import basic stuff
from typing import Optional

# import own stuff
import tlsmate.client_state as client_state
import tlsmate.ext as ext
import tlsmate.pdu as pdu
import tlsmate.tls as tls

import pytest

# import other stuff


def decode_extension(
    octets: bytes,
    session: client_state.SessionState,
    msg: Optional[tls.MessageId] = None,
) -> ext.Extension:
    session.xcoding_msg = msg or tls.MessageId.SERVER_HELLO
    decoder = pdu.Decoder(octets)
    obj = ext.decode_extension(decoder, session)
    assert decoder.get_length() == 0
    return obj


def encode_extension(
    extension: ext.Extension,
    session: client_state.SessionState,
    msg: Optional[tls.MessageId] = None,
) -> ext.Extension:
    session.xcoding_msg = msg or tls.MessageId.CLIENT_HELLO
    encoder = pdu.Encoder()
    extension._encode_extension(encoder, session)
    return encoder.get_octet_string()


def test_ServerNameIndication(session):
    data = bytes.fromhex("00 00 00 0e 00 0c 00 00 09 6c 6f 63 61 6c 68 6f 73 74")
    sni = decode_extension(data, session)
    assert sni.server_name_list[0].name_type is tls.SniHostType.HOST_NAME
    assert sni.server_name_list[0].name == "localhost"
    assert sni.get_host_name() == "localhost"

    sni = ext.ServerNameIndication(host_name="localhost")
    assert encode_extension(sni, session) == data


def test_sni_empty(session):
    data = bytes.fromhex("00 00 00 00")
    sni = decode_extension(data, session)
    assert len(sni.server_name_list) == 0


def test_sni_wrong_length(session):
    data = bytes.fromhex("00 00 00 0e 00 0d 00 00 09 6c 6f 63 61 6c 68 6f 73 74")
    with pytest.raises(tls.ServerMalfunction) as exc:
        decode_extension(data, session)

    assert exc.value.malfunction.issue is tls.ServerIssue.LENGTH_TOO_SHORT
    assert exc.value.malfunction.extension is tls.Extension.SERVER_NAME


def test_sni_wrong_name_type(session):
    data = bytes.fromhex("00 00 00 0e 00 0c 01 00 09 6c 6f 63 61 6c 68 6f 73 74")
    sni = decode_extension(data, session)
    assert sni.server_name_list[0].name_type == 1
    assert sni.server_name_list[0].name == "localhost"
    assert sni.get_host_name() is None


def test_ems_ok(session):
    data = bytes.fromhex("00 17 00 00")
    etm = decode_extension(data, session)
    assert isinstance(etm, ext.ExtendedMasterSecret)
    assert encode_extension(ext.ExtendedMasterSecret(), session) == data


def test_ems_wrong_length(session):
    data = bytes.fromhex("00 17 00 01 00")
    with pytest.raises(tls.ServerMalfunction) as exc:
        decode_extension(data, session)

    assert exc.value.malfunction.issue is tls.ServerIssue.EXTENSION_LENGTH_TOO_LONG


def test_etm_ok(session):
    data = bytes.fromhex("00 16 00 00")
    etm = decode_extension(data, session)
    assert isinstance(etm, ext.EncryptThenMac)
    assert encode_extension(ext.EncryptThenMac(), session) == data


def test_etm_wrong_length(session):
    data = bytes.fromhex("00 16 00 01 00")
    with pytest.raises(tls.ServerMalfunction) as exc:
        decode_extension(data, session)

    assert exc.value.malfunction.issue is tls.ServerIssue.EXTENSION_LENGTH_TOO_LONG


def test_renegotiation_info_ok(session):
    data = bytes.fromhex("ff 01 00 02 01 00")
    assert encode_extension(ext.RenegotiationInfo(), session) == data
    data = bytes.fromhex("ff 01 00 04 03 ab cd ef")
    reneg = decode_extension(data, session)
    assert isinstance(reneg, ext.RenegotiationInfo)
    assert reneg.renegotiated_connection == b"\xab\xcd\xef"


def test_ec_point_format(session):
    epf = ext.EcPointFormats(ec_point_formats=[tls.EcPointFormat.UNCOMPRESSED, 5])
    assert encode_extension(epf, session) == bytes.fromhex("00 0b 00 03 02 00 05")


def test_ec_point_format_wrong_length(session):
    data = bytes.fromhex("00 0b 00 03 03 00 05")
    with pytest.raises(tls.ServerMalfunction) as exc:
        decode_extension(data, session)

    assert exc.value.malfunction.issue is tls.ServerIssue.LENGTH_TOO_SHORT


def test_supported_groups_unknown_value(session):
    data = bytes.fromhex("00 0a 00 0a 00 08 00 1d 00 17 00 18 00 aa")

    groups = decode_extension(data, session)
    assert type(groups) is ext.SupportedGroups
    assert groups.named_curve_list == [
        tls.SupportedGroups.X25519,
        tls.SupportedGroups.SECP256R1,
        tls.SupportedGroups.SECP384R1,
        0x00AA,
    ]
    supp = ext.SupportedGroups(
        supported_groups=[
            tls.SupportedGroups.X25519,
            tls.SupportedGroups.SECP256R1,
            tls.SupportedGroups.SECP384R1,
            0x00AA,
        ]
    )
    assert encode_extension(supp, session) == data


def test_supported_groups_alternative():
    groups = [
        tls.SupportedGroups.X25519,
        tls.SupportedGroups.SECP256R1,
        tls.SupportedGroups.SECP384R1,
        0x00AA,
    ]
    s1 = ext.SupportedGroups(supported_groups=groups)
    s2 = ext.SupportedGroups(named_group_list=groups)
    assert s1 == s2
    assert s1.named_curve_list == groups


def test_sig_algo(session):
    extension = ext.SignatureAlgorithms(
        signature_algorithms=[tls.SignatureScheme.ECDSA_SHA1, 0xAABB]
    )

    data = encode_extension(extension, session)
    assert data == bytes.fromhex("00 0d 00 06 00 04 02 03 aa bb")

    sig_algo = decode_extension(data, session)
    assert type(sig_algo) is ext.SignatureAlgorithms
    assert sig_algo.supported_signature_algorithms == [
        tls.SignatureScheme.ECDSA_SHA1,
        0xAABB,
    ]


def test_heartbeat(session):
    data = bytes.fromhex("00 0f 00 01 11")
    extension = ext.Heartbeat(heartbeat_mode=0x11)
    assert encode_extension(extension, session) == data

    extension2 = ext.Heartbeat(mode=0x11)
    assert extension == extension2

    hb = decode_extension(data, session)
    assert hb.mode == hb.heartbeat_mode == 0x11

    extension = ext.Heartbeat(heartbeat_mode=tls.HeartbeatMode.PEER_ALLOWED_TO_SEND)
    assert extension.mode is tls.HeartbeatMode.PEER_ALLOWED_TO_SEND


def test_cert_authorities(session):
    data = bytes.fromhex("00 2f 00 0e 00 0c 00 03 aa bb cc 00 05 11 22 33 44 55")
    ca = decode_extension(data, session)
    assert type(ca) is ext.CertificateAuthorities
    assert len(ca.authorities) == 2
    assert ca.authorities[0] == bytes.fromhex("aa bb cc")
    assert ca.authorities[1] == bytes.fromhex("11 22 33 44 55")


def test_session_ticket(session):
    data = bytes.fromhex("00 23 00 03 01 02 03")
    ticket = ext.SessionTicket(ticket=b"\01\02\03")
    assert encode_extension(ticket, session) == data

    ticket = decode_extension(data, session)
    assert isinstance(ticket, ext.SessionTicket)
    assert ticket.ticket == b"\01\02\03"


def test_status_request(session):
    extension = ext.StatusRequest(
        responder_ids=[b"\4\5"], extensions=bytes.fromhex("00 17 00 00")
    )
    data = bytes.fromhex("00 05 00 0d 01  00 04 00 02 04 05  00 04 00 17 00 00")
    assert encode_extension(extension, session) == data


def test_status_request_v2(session):
    extension = ext.StatusRequestV2(
        responder_ids=[b"\4\5"], extensions=bytes.fromhex("00 17 00 00")
    )
    data = bytes.fromhex(
        "00 11 00 11 00 0f 01 00 0c 00 04 00 02 04 05  00 04 00 17 00 00"
    )
    assert encode_extension(extension, session) == data


def test_status_request_v2_deserialize(session):
    data = bytes.fromhex(
        "00 11 00 11 00 0f 01 00 0c 00 04 00 02 04 05  00 04 00 17 00 00"
    )
    status = decode_extension(data, session)
    assert type(status) is ext.StatusRequestV2
    assert len(status.certificate_status_req_list) == 1
    req_item = status.certificate_status_req_list[0]
    assert req_item.status_type is tls.StatusType.OCSP
    assert req_item.request.responder_id_list == [b"\4\5"]
    assert req_item.request.request_extensions == bytes.fromhex("00 17 00 00")


def test_supported_versions_client(session):
    data = bytes.fromhex("00 2b 00 05 04 03 03 aa ff")
    version_list = [tls.Version.TLS12, 0xAAFF]
    versions = ext.SupportedVersions(versions=version_list)
    assert encode_extension(versions, session, tls.MessageId.CLIENT_HELLO) == data
    versions = decode_extension(data, session, tls.MessageId.CLIENT_HELLO)
    assert versions.versions == [tls.Version.TLS12, 0xAAFF]


def test_supported_versions_server(session):
    data = bytes.fromhex("00 2b 00 02 03 03")
    version = decode_extension(data, session)
    assert isinstance(version, ext.SupportedVersions)
    assert version.selected_version is tls.Version.TLS12


def test_cookie(session):
    data = bytes.fromhex("00 2c 00 05 00 03 01 02 03")
    cookie = ext.Cookie(cookie=b"\01\02\03")
    assert encode_extension(cookie, session) == data

    cookie = decode_extension(data, session)
    assert isinstance(cookie, ext.Cookie)
    assert cookie.cookie == b"\01\02\03"


def test_key_share_client(session):
    data = bytes.fromhex(
        "00 33 00 6b 00 69 00 1d 00 20 7e b1 50 16 ac 1c "
        "c0 51 6f 90 42 6f 13 0c 3b 22 bd c3 5e 16 f4 88 "
        "50 77 80 c5 e3 6c f9 43 ad 4f 00 17 00 41 04 cf "
        "09 bf 71 8b 4f fa 62 ac 8c bb f4 be 9d 06 3e 89 "
        "46 78 02 1b d2 bd 1e 9d 57 c5 42 10 05 91 35 f3 "
        "b6 2a 52 4d 0d 2b e4 86 22 74 c6 9f 72 4e 89 18 "
        "65 24 48 d6 32 eb b5 36 c8 c0 dd 50 88 01 08"
    )
    share1 = ext.KeyShareEntry(
        group=tls.SupportedGroups.X25519,
        key_exchange=b'~\xb1P\x16\xac\x1c\xc0Qo\x90Bo\x13\x0c;"\xbd\xc3^\x16\xf4\x88Pw\x80\xc5\xe3l\xf9C\xadO',  # noqa
    )
    share2 = ext.KeyShareEntry(
        group=tls.SupportedGroups.SECP256R1,
        key_exchange=b'\x04\xcf\t\xbfq\x8bO\xfab\xac\x8c\xbb\xf4\xbe\x9d\x06>\x89Fx\x02\x1b\xd2\xbd\x1e\x9dW\xc5B\x10\x05\x915\xf3\xb6*RM\r+\xe4\x86"t\xc6\x9frN\x89\x18e$H\xd62\xeb\xb56\xc8\xc0\xddP\x88\x01\x08',  # noqa
    )
    key_share = ext.KeyShare(client_shares=[share1, share2])
    assert encode_extension(key_share, session, tls.MessageId.CLIENT_HELLO) == data


def test_key_share_server(session):
    data = bytes.fromhex(
        "00 33 00 45 00 17 00 41 04 67 1c ff 0e 70 c2 41 "
        "74 6a 1f 38 f9 74 e9 fa 0e 49 40 8c 37 fa 09 58 "
        "36 33 d6 6e ea 4a d9 a2 28 4e 1a cd 40 99 7c e2 "
        "c1 76 28 cc 97 e5 02 b5 c7 08 62 a8 bc dd 00 f6 "
        "aa ab fc df f3 2d 25 4c ad "
    )
    key_share = decode_extension(data, session, tls.MessageId.SERVER_HELLO)
    assert isinstance(key_share, ext.KeyShare)
    assert key_share.server_share.group is tls.SupportedGroups.SECP256R1
    assert (
        key_share.server_share.key_exchange
        == b"\x04g\x1c\xff\x0ep\xc2Atj\x1f8\xf9t\xe9\xfa\x0eI@\x8c7\xfa\tX63\xd6n\xeaJ\xd9\xa2(N\x1a\xcd@\x99|\xe2\xc1v(\xcc\x97\xe5\x02\xb5\xc7\x08b\xa8\xbc\xdd\x00\xf6\xaa\xab\xfc\xdf\xf3-%L\xad"  # noqa
    )


def test_pre_shared_keys(session):
    psk = ext.PreSharedKey(
        identities=[
            ext.PskIdentity(identity=b"\01\02\03", obfuscated_ticket_age=0xAFFEDEAD)
        ],
        binders=[b"\01\02\03\04"],
    )
    assert (
        encode_extension(psk, session)
        == b"\x00)\x00\x12\x00\t\x00\x03\x01\x02\x03\xaf\xfe\xde\xad\x00\x05\x04\x01\x02\x03\x04"  # noqa
    )
    assert psk.encode_binders() == b"\x00\x05\x04\x01\x02\x03\x04"


def test_psk_exchange_mode(session):
    psk_exchange_mode = ext.PskKeyExchangeMode(
        ke_modes=[tls.PskKeyExchangeMode.PSK_DHE_KE, 5]
    )  # noqa
    assert encode_extension(psk_exchange_mode, session) == b"\x00-\x00\x03\x02\x01\x05"


def test_early_data_client_hello(session):
    data = bytes.fromhex("00 2a 00 00")
    ed = ext.EarlyData()
    assert encode_extension(ed, session, tls.MessageId.CLIENT_HELLO) == data

    ed = decode_extension(data, session, tls.MessageId.CLIENT_HELLO)
    assert isinstance(ed, ext.EarlyData)


def test_early_data_new_session_ticket(session):
    data = bytes.fromhex("00 2a 00 04 00 00 01 00")
    ed = ext.EarlyData(max_early_data_size=256)
    assert encode_extension(ed, session, tls.MessageId.NEW_SESSION_TICKET) == data

    ed = decode_extension(data, session, tls.MessageId.NEW_SESSION_TICKET)
    assert isinstance(ed, ext.EarlyData)
    assert ed.max_early_data_size == 256


def test_post_handshake_auth(session):
    data = bytes.fromhex("00 31 00 00")
    pha = decode_extension(data, session)
    assert isinstance(pha, ext.PostHandshakeAuth)
    assert encode_extension(ext.PostHandshakeAuth(), session) == data


def test_alpn(session):
    data = b"\x00\x10\x00\x16\x00\x14\x08http/1.1\x05local\x04host"
    alpn = ext.ApplLayerProtNeg(protocols=[tls.Alpn.HTTP11, "local", b"host"])
    assert encode_extension(alpn, session) == data
    alpn = decode_extension(data, session)
    assert isinstance(alpn, ext.ApplLayerProtNeg)
    assert alpn.protocol_name_list == [tls.Alpn.HTTP11, b"local", b"host"]


def test_key_share_hello_retry(session):
    data = bytes.fromhex("00 33 00 02 00 17")
    key_share = decode_extension(data, session, tls.MessageId.HELLO_RETRY_REQUEST)
    assert isinstance(key_share, ext.KeyShare)
    assert key_share.selected_group is tls.SupportedGroups.SECP256R1


def test_unknown_ext(session):
    data = bytes.fromhex("00 ab 00 0e 00 0c 00 00 09 6c 6f 63 61 6c 68 6f 73 74")
    extension = decode_extension(data, session)
    assert type(extension) is ext.UnknownExtension
    assert extension.id == 0x00AB
    assert extension.bytes == data[4:]
    extension = ext.UnknownExtension(id=0x00AB, bytes=data[4:])
    assert encode_extension(extension, session) == data
