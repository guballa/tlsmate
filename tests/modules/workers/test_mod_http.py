# -*- coding: utf-8 -*-
"""Implements a class to test the test profile worker.
"""
from tlsmate.workers.http import _analyse_response, _make_http_response
from tlsmate.server_profile import SPObject
from tlsmate import tls


def test_http_response():
    data = (
        b"HTTP/1.1 302 Found\r\n"
        b"date: Thu, 17 Mar 2022 18:42:48 GMT\r\n"
        b"content-type: text/html; charset=UTF-8\r\n"
        b"server: nginx\r\n"
        b"location: https://guballa.de/startseite\r\n"
        b"strict-transport-security: max-age=31536000; includeSubDomains; preload\r\n"
    )
    response = _make_http_response(data)
    prof = SPObject()
    _analyse_response(prof, response)
    assert prof.valid_http_response is tls.ScanState.TRUE
    assert prof.status_code == 302
    assert prof.reason == "Found"
    assert prof.server == "nginx"
    assert prof.http_forwarding == "https://guballa.de/startseite"
    assert prof.hsts.include_subdomain is tls.ScanState.TRUE
    assert prof.hsts.preload is tls.ScanState.TRUE
    assert prof.hsts.max_age == 31536000
