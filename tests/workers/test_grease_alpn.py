# -*- coding: utf-8 -*-
"""Implements a class to test the grease for alpn.
"""
import pathlib
from tlsmate.workers.grease import ScanGrease
from tlsmate.tlssuite import TlsSuiteTester
from tlsmate.tlssuite import TlsLibrary


class TestCase(TlsSuiteTester):
    """Class used for tests with pytest.

    For more information refer to the documentation of the TcRecorder class.
    """

    sp_in_yaml = "profile_alpn_openssl3_0_0"
    recorder_yaml = "recorder_grease_alpn"
    path = pathlib.Path(__file__)
    server_cmd = (
        "utils/start_openssl --version {library} --port {server_port} "
        "--cert1 server-rsa --cert2 server-ecdsa "
        "-- -www -cipher ALL -alpn h2,spdy/2,h3"
    )
    library = TlsLibrary.openssl3_0_0

    server = "localhost"

    def run(self, tlsmate, is_replaying):
        server_profile = tlsmate.server_profile
        ScanGrease(tlsmate).run()
        profile = server_profile.make_serializable()
        grease_prof = profile["features"]["grease"]
        assert grease_prof["alpn_tolerance"] == "TRUE"


if __name__ == "__main__":
    TestCase().entry(is_replaying=False)
