# -*- coding: utf-8 -*-
"""Implements a class to test the resumption worker.
"""
import pathlib
from tlsmate.workers.http import ScanHttp
from tlsmate.tlssuite import TlsSuiteTester
from tlsmate.tlssuite import TlsLibrary


class TestCase(TlsSuiteTester):
    """Class used for tests with pytest.

    For more information refer to the documentation of the TcRecorder class.
    """

    sp_in_yaml = "profile_sig_algos_openssl3_0_0"
    recorder_yaml = "recorder_http"
    path = pathlib.Path(__file__)
    server_cmd = (
        "utils/start_openssl --version {library} --port {server_port} "
        "--cert1 server-rsa --cert2 server-ecdsa "
        "-- -www"
    )
    library = TlsLibrary.openssl3_0_0

    server = "localhost"

    def run(self, tlsmate, is_replaying):
        server_profile = tlsmate.server_profile
        ScanHttp(tlsmate).run()
        profile = server_profile.make_serializable()
        http = profile["http"]
        assert http["reason"] == "ok"
        assert http["status_code"] == 200
        assert http["valid_http_response"] == "TRUE"


if __name__ == "__main__":
    TestCase().entry(is_replaying=False)
