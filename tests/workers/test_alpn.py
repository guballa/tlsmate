# -*- coding: utf-8 -*-
"""Implements a class to test the resumption worker.
"""
import pathlib
from tlsmate.workers.alpn import ScanAlpn
from tlsmate.tlssuite import TlsSuiteTester
from tlsmate.tlssuite import TlsLibrary


class TestCase(TlsSuiteTester):
    """Class used for tests with pytest.

    For more information refer to the documentation of the TcRecorder class.
    """

    sp_in_yaml = "profile_sig_algos_openssl3_0_0"
    sp_out_yaml = "profile_alpn_openssl3_0_0"
    recorder_yaml = "recorder_alpn"
    path = pathlib.Path(__file__)
    server_cmd = (
        "utils/start_openssl --version {library} --port {server_port} "
        "--cert1 server-rsa --cert2 server-ecdsa "
        "-- -www -cipher ALL -alpn h2,spdy/2,h3"
    )
    library = TlsLibrary.openssl3_0_0

    server = "localhost"

    def run(self, tlsmate, is_replaying):
        server_profile = tlsmate.server_profile
        ScanAlpn(tlsmate).run()
        profile = server_profile.make_serializable()
        alpn = profile["features"]["alpn"]
        assert alpn["extension_supported"] == "TRUE"
        assert alpn["server_preference"] == "TRUE"
        assert alpn["protocols"] == ["h2", "spdy/2", "h3"]


if __name__ == "__main__":
    TestCase().entry(is_replaying=False)
