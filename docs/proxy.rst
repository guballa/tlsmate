.. _httpproxysupport:

HTTP proxy support
==================

TLS connections over an HTTP proxy are supported by ``tlsmate`` as well.

The proxy configuration is set by the OS environment variable ``http_proxy``,
for example:

.. code-block:: console

   $ http_proxy=http://localhost:3128

.. note::
   The protocol and the port (``http`` and ``3128`` in the example above) must
   be specified as shown.

In case a proxy is used, the domain name resoution will be done by using "DNS
over HTTPS" (DoH). In this case the following hardcoded DNS servers will be
used (given in the order of their priority):

* ``https://dns.google/dns-query``
* ``https://cloudflare-dns.com/dns-query``
* ``https://dns.quad9.net/dns-query``
* ``https://dns.digitale-gesellschaft.ch/dns-query``

