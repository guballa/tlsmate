Features
========

The TLS server scanner plugin examines a TLS server and provides information
about the TLS configuration, the certificate chains and various vulnerabilities
which the server might suffer from.

The plugin scans for the following items:

* support for DNS-CAA

* TLS versions SSLv2 - TLS1.3

* per TLS protocol version all supported cipher suites are provided, including
  an indication if the server enforces its own preference

* per TLS protocol version all supported named groups are provided, including
  an indication if the server enforces its own preference

* per TLS protocol version all supported signature algorithms are provided

* the Diffie-Hellman (DH) groups are provided

* information about the following TLS features are provided:

  * support for OSCP stapling
  * support for multi-OCSP stapling
  * support for heartbeat
  * support for downgrade attack prevention
  * support for compression
  * support for encrypt-then-mac
  * support for extended-master-secret
  * support for insecure renegotiation
  * support for secure renegotiation (via extension or via signalling cipher
    suite value (SCSV)
  * support for resumption via session-id
  * support for resumption via session tickets
  * support for resumption with PSKs (TLS1.3)
  * tolerance for unknown protocol values (GREASE)
  * reuse of ephemeral keys (DHE and ECDHE)
  * support for early-data (TLS1.3)

* information about HTTP responses are provided, including the http strict
  transport security header (HSTS)

* all certificate chains are provided with an indication for their validity

* vulnerabilities:

  * BEAST
  * CCS-injection
  * CRIME
  * FREAK
  * Heartbleed
  * Logjam
  * ROBOT
  * Sweet-32
  * various CBC-padding oracles, like POODLE, TLS-POODLE, Lucky-Minus-20, etc.)

In addition the scanner supports the following features:

* scanning via an :ref:`HTTP-proxy <httpproxysupport>`.
* configuring a customized trust store
* client authentication
* output as colored console text, yaml or JSON
* configuring the style for the console output
