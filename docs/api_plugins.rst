Plugins
-------

Plugin
^^^^^^
.. autoclass:: tlsmate.plugin.Plugin
    :noindex:

Worker
^^^^^^
.. autoclass:: tlsmate.plugin.Worker
    :noindex:
