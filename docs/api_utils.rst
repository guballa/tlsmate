Utilities
---------

Helper classes
^^^^^^^^^^^^^^
.. autofunction:: tlsmate.utils.Table

Helper methods
^^^^^^^^^^^^^^
.. autofunction:: tlsmate.utils.fold_string
.. autofunction:: tlsmate.utils.filter_cipher_suites

.. autofunction:: tlsmate.pdu.pack_uint8
.. autofunction:: tlsmate.pdu.pack_uint16
.. autofunction:: tlsmate.pdu.pack_uint32
.. autofunction:: tlsmate.pdu.pack_uint64
.. autofunction:: tlsmate.pdu.dump
.. autofunction:: tlsmate.pdu.dump_short
.. autofunction:: tlsmate.pdu.string
