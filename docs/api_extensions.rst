TLS extensions
--------------

Unknown Extension
^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.UnknownExtension

ServerNameIndication
^^^^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.ServerNameIndication
.. autoclass:: tlsmate.ext.ServerName

ExtendedMasterSecret
^^^^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.ExtendedMasterSecret

EncryptThenMac
^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.EncryptThenMac

RenegotiationInfo
^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.RenegotiationInfo

EcPointFormats
^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.EcPointFormats

SupportedGroups
^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.SupportedGroups

SignatureAlgorithms
^^^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.SignatureAlgorithms

Heartbeat
^^^^^^^^^
.. autoclass:: tlsmate.ext.Heartbeat
    :noindex:

CertificateAuthorities
^^^^^^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.CertificateAuthorities

SessionTicket
^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.SessionTicket

StatusRequest
^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.StatusRequest
.. autoclass:: tlsmate.ext.OCSPStatusRequest

StatusRequestV2
^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.StatusRequestV2
.. autoclass:: tlsmate.ext.CertificateStatusRequestItemV2

SupportedVersions
^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.SupportedVersions

Cookie
^^^^^^
.. autoclass:: tlsmate.ext.Cookie

KeyShare
^^^^^^^^
.. autoclass:: tlsmate.ext.KeyShare
.. autoclass:: tlsmate.ext.KeyShareEntry

PreSharedKey
^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.PreSharedKey
.. autoclass:: tlsmate.ext.PskIdentity

PskKeyExchangeMode
^^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.PskKeyExchangeMode

EarlyData
^^^^^^^^^
.. autoclass:: tlsmate.ext.EarlyData

PostHandshakeAuth
^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.PostHandshakeAuth

ApplLayerProtNeg
^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.ext.ApplLayerProtNeg
