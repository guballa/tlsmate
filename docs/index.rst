.. tlsmate documentation master file

Welcome to tlsmate's documentation!
===================================

This project implements a TLS testing framework. A TLS scanner is provided,
too.

The project is hosted on `GitLab <https://gitlab.com/guballa/tlsmate>`_.

.. toctree::
   :caption: Quick start
   :maxdepth: 3

   overview
   installation
   miniconfig
   usage

.. toctree::
   :caption: Framework
   :maxdepth: 3

   tlsfeatures
   ipv6
   proxy
   cli_config

.. toctree::
   :caption: TLS server scanner
   :maxdepth: 3

   scanner
   scanner_output
   cli
   style

.. toctree::
   :caption: Extending tlsmate
   :maxdepth: 3

   plain_python
   plugin
   class_description
   testcase
   api


..
    Indices and tables
    ==================

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
