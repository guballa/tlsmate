IP version handling
===================

In case the server is specified by its hostname, ``tlsmate`` will perform a
domain name resolution via DNS and will use the same IP address for all
subsequent connections to this server.

If the domain name resolution results in both IPv4 and IPv6 addresses, then by
default ``tlsmate`` will use the first IPv4 address found in the DNS response.
This behavior can be overruled by the command line option ``--ipv6``, which
forces the use the first found IPv6 address instead. Respectively, the option
``--no-ipv6`` enforces the use of an IPv4 address. If either of these options
is given, but the domain name does not resolve into the desired IP address
version, the command will fail.

If a specific IP address shall be used, it must be specified as the hostname.
In this case no DNS query will be performed.

.. note::
   If an IP address is given as a hostname, it might be required to provide the
   domain name through the ``--sni`` command line option.

