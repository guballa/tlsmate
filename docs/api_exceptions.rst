Exceptions
----------

Base classes
^^^^^^^^^^^^

.. autoclass:: tlsmate.tls.TlsmateException
.. autoclass:: tlsmate.tls.Malfunction


Exceptions used by tlsmate
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoexception:: tlsmate.tls.ServerMalfunction
.. autoexception:: tlsmate.tls.TlsConnectionClosedError
.. autoexception:: tlsmate.tls.TlsMsgTimeoutError
.. autoexception:: tlsmate.tls.CurveNotSupportedError
.. autoexception:: tlsmate.tls.ScanError
.. autoexception:: tlsmate.tls.OcspError
.. autoexception:: tlsmate.tls.UntrustedCertificate
.. autoexception:: tlsmate.tls.ServerParmsSignatureInvalid
.. autoexception:: tlsmate.tls.UserInputError
