Application classes
-------------------

Class TlsMate
^^^^^^^^^^^^^

.. autoclass:: tlsmate.tlsmate.TlsMate
   :member-order: alphabetical

Class TrustStore
^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.trust_store.TrustStore
   :member-order: alphabetical

Class ConfigItem
^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.structs.ConfigItem
   :member-order: alphabetical

Class Configuration
^^^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.config.Configuration
   :member-order: alphabetical
