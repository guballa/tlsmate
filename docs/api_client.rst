Client classes
--------------

Class ClientProfile
^^^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.client_state.ClientProfile
   :member-order: alphabetical
   :noindex:

Class SessionState
^^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.client_state.SessionState
   :member-order: alphabetical

Class Client
^^^^^^^^^^^^

.. autoclass:: tlsmate.client.Client
   :member-order: alphabetical

Class TlsConnection
^^^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.connection.TlsConnection
   :member-order: alphabetical

Class TlsConnectionMsgs
^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.connection.TlsConnectionMsgs
   :member-order: alphabetical
