TLS message classes
-------------------

Any
^^^
.. autoclass:: tlsmate.msg.Any

Timeout
^^^^^^^
.. autoclass:: tlsmate.msg.Timeout

HelloRequest
^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.HelloRequest

ClientHello
^^^^^^^^^^^
.. autoclass:: tlsmate.msg.ClientHello

ServerHello
^^^^^^^^^^^
.. autoclass:: tlsmate.msg.ServerHello

HelloRetryRequest
^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.HelloRetryRequest

Certificate
^^^^^^^^^^^
.. autoclass:: tlsmate.msg.Certificate
.. autoclass:: tlsmate.msg.CertificateEntry

CertificateVerify
^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.CertificateVerify

CertificateRequest
^^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.CertificateRequest

CertificateStatus
^^^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.CertificateStatus

ServerKeyExchange
^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.ServerKeyExchange
.. autoclass:: tlsmate.msg.ECCurve
.. autoclass:: tlsmate.msg.ECPoint
.. autoclass:: tlsmate.msg.ECParameters
.. autoclass:: tlsmate.msg.ServerECDHParams
.. autoclass:: tlsmate.msg.ServerDHParams
.. autoclass:: tlsmate.msg.ServerRSAParams

ServerHelloDone
^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.ServerHelloDone

ClientKeyExchange
^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.ClientKeyExchange

Finished
^^^^^^^^
.. autoclass:: tlsmate.msg.Finished

EndOfEarlyData
^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.EndOfEarlyData

NewSessionTicket
^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.NewSessionTicket

EncryptedExtensions
^^^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.EncryptedExtensions

ChangeCipherSpec
^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.ChangeCipherSpec

Alert
^^^^^
.. autoclass:: tlsmate.msg.Alert

AppData
^^^^^^^
.. autoclass:: tlsmate.msg.AppData

HeartbeatRequest
^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.HeartbeatRequest

HeartbeatResponse
^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.HeartbeatResponse

SSL2 ClientHello
^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.SSL2ClientHello

SSL2 SSL2ServerHello
^^^^^^^^^^^^^^^^^^^^
.. autoclass:: tlsmate.msg.SSL2ServerHello

SSL2 Error
^^^^^^^^^^
.. autoclass:: tlsmate.msg.SSL2Error
