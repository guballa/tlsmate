Enumerations
------------

Base classes
^^^^^^^^^^^^

.. autoclass:: tlsmate.tls.ExtendedEnum
.. autoclass:: tlsmate.tls.ExtendedIntEnum

TLS messages enumerations
^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.tls.ContentType
.. autoclass:: tlsmate.tls.HandshakeType
.. autoclass:: tlsmate.tls.CCSType
.. autoclass:: tlsmate.tls.HeartbeatType
.. autoclass:: tlsmate.tls.Version
.. autoclass:: tlsmate.tls.CompressionMethod
.. autoclass:: tlsmate.tls.CipherSuite

Alert message enumerations
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.tls.AlertDescription
.. autoclass:: tlsmate.tls.AlertLevel

SSL2 enumerations
^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.tls.SSLMessageType
.. autoclass:: tlsmate.tls.SSLVersion
.. autoclass:: tlsmate.tls.SSLCipherKind
.. autoclass:: tlsmate.tls.SSLError

Extension related enumerations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.tls.Extension
.. autoclass:: tlsmate.tls.Alpn
.. autoclass:: tlsmate.tls.EcCurveType
.. autoclass:: tlsmate.tls.EcPointFormat
.. autoclass:: tlsmate.tls.HeartbeatMode
.. autoclass:: tlsmate.tls.PskKeyExchangeMode
.. autoclass:: tlsmate.tls.SignatureScheme
.. autoclass:: tlsmate.tls.SniHostType
.. autoclass:: tlsmate.tls.StatusType
.. autoclass:: tlsmate.tls.SupportedGroups


Server profile related enumerations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: tlsmate.tls.ScanState
.. autoclass:: tlsmate.tls.HeartbeatState
